#include "user_common_cpp.h"
#include "stepper.h"
#include "temperature.h"
#include "machinecustom.h"
#include "controlxyz.h"
#include "planner.h"
#include "planner_running_status.h"
#if defined(STM32F429xx)
  #include "process_command.h"
  #include "process_m_code.h"
#elif defined(STM32F407xx)
  #include "guicontrol.h"
#endif
#include "functioncustom.h"
#include "ConfigurationStore.h"
#include "gcode.h"

#ifdef HAS_FILAMENT
unsigned char m83_process = 0;
namespace feature_filament
{

  static uint8_t startLoadFlag;               /*!< 开始进丝标志位 */
  static uint8_t startUnloadFlag;             /*!< 开始退丝标志位 */
  static uint8_t timeOutFlag;                 /*!< 进退丝超时标志位 */
  static unsigned long timeOutTickCount;      /*!< 进退丝超时计数 */
  static bool is_process_load_unload_done = false;
  static volatile bool is_load_diff_eb = false;
  bool Lowtemp_load = false;
  // 重置进退丝状态
  void control_reset_status(void)
  {
    // 设置目标温度为0
    sg_grbl::temperature_set_extruder_target((float)0, 0);
    ccm_param.t_gui.target_nozzle_temp[0] = 0;
    #if defined(STM32F429xx)

    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL)
    {
      sg_grbl::temperature_set_extruder_target((float)0, 1);
      ccm_param.t_gui.target_nozzle_temp[1] = 0;
    }

    #endif
    // 重置进退丝状态变量
    startLoadFlag = 0;
    startUnloadFlag = 0;
    timeOutFlag = 0;
    timeOutTickCount = 0;
    is_process_load_unload_done = false;
  }

  static void _control_exit(bool isCancel)
  {
    #if defined(STM32F429xx)

    // 串口上传信息到上位机2017.7.6
    if (!isCancel)
      USER_EchoLogStr("M701 M702 finish\r\n");

    #endif
    // 退出进丝或退丝操作
    user_send_internal_cmd("G1 F2400"); // 设置速度为7200mm/min
    user_send_internal_cmd("M82");// 关闭绝对模式
    user_send_internal_cmd("G92 E0 B0"); // 重置E、B坐标值为0
    // 重置进退丝状态
    control_reset_status();
  }

  // 准备进退丝操作
  static void _control_prepare(void)
  {
    #if defined(STM32F429xx)
    ccm_param.t_gui_p.IsSuccessFilament = 0;
    ccm_param.t_gui_p.IsFinishedFilamentHeat = 0;
    sg_grbl::temperature_set_extruder_target((float)sg_grbl::filament_load_unload_temp, 0);
    ccm_param.t_gui.target_nozzle_temp[0] = sg_grbl::filament_load_unload_temp;
    gcode::active_extruder = 0;

    // 喷嘴预热230度
    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL)
    {
      sg_grbl::temperature_set_extruder_target((float)sg_grbl::filament_load_unload_temp, 1);
      ccm_param.t_gui.target_nozzle_temp[1] = sg_grbl::filament_load_unload_temp;
    }

    if (!ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE)                                                         // 进退丝，平台未归零，先归零
    {
      user_send_internal_cmd((char *)"G28");           // xyz归零
    }

    if (sg_grbl::st_get_position_length(X_AXIS) > 0.0f || sg_grbl::st_get_position_length(Y_AXIS) > 0.0f)               // XY是否在零点
    {
      if (P2_Pro_NEW == ccm_param.t_sys_data_current.model_id || P3_Pro == ccm_param.t_sys_data_current.model_id)
      {
        user_send_internal_cmd((char *)"M2003 S0");    // 关闭坐标转换
        user_send_internal_cmd((char *)"G1 F2400 X0 Y0"); // 移动XY到零点
        user_send_internal_cmd((char *)"M2003 S1");    // 开启坐标转换
      }
      else
      {
        xy_to_zero();
      }
    }

    if (sg_grbl::st_get_position_length(Z_AXIS) < 50.0f)                                                // 获取平台当前实际高度，小于50mm，下降到50mm位置
    {
      user_send_internal_cmd((char *)"M2003 S0");    // 关闭坐标转换
      user_send_internal_cmd((char *)"G1 F600 Z50");   // z下降到50mm位置
      user_send_internal_cmd((char *)"M2003 S1");    // 开启坐标转换
    }

    user_send_internal_cmd((char *)"M83");             // 设置喷嘴E、B为绝对模式
    #elif defined(STM32F407xx)
    ccm_param.t_gui_p.IsSuccessFilament = 0;
    ccm_param.t_gui_p.IsFinishedFilamentHeat = 0;
    GUI_WNozzleTargetTemp(sg_grbl::filament_load_unload_temp);// 喷嘴预热230度

    //X5机器进丝时EB电机要先转动一点以卡住耗材
    if (K5 == ccm_param.t_sys_data_current.model_id && ccm_param.t_sys_data_current.enable_color_mixing && Lowtemp_load)
    {
      user_send_internal_cmd("G92 E0 B0 isInternal");
      user_send_internal_cmd("G1 F200 E3 B3 isInternal");
    }

    if (0U == ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE) // 进退丝，平台未归零，先归零
    {
      user_send_internal_cmd("G28 isInternal");// xyz归零

      while (0U == ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE) // 等待归零完成
      {
        osDelay(50);
      }
    }

    if ((sg_grbl::st_get_position_length(X_AXIS) > 0.0F) || (sg_grbl::st_get_position_length(Y_AXIS) > 0.0F)) // XY是否在零点
    {
      xy_to_zero();
      sg_grbl::st_synchronize();
    }

    if (sg_grbl::st_get_position_length(Z_AXIS) < 50.0F || (ccm_param.t_sys_data_current.IsMechanismLevel ? (!ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE) : 0))                                                // 获取平台当前实际高度，小于50mm，下降到50mm位置
    {
      user_send_internal_cmd("G1 F600 Z50 I0 H0 isInternal"); // z下降到50mm位置
    }

    #endif
  }

  // 执行进丝操作
  static void _control_process_load(void)
  {
    #if defined(STM32F429xx)
    bool is_heating_done = (int)sg_grbl::temperature_get_extruder_current(0) >= (ccm_param.t_gui.target_nozzle_temp[0] - 3);

    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL)
    {
      is_heating_done = is_heating_done && (int)sg_grbl::temperature_get_extruder_current(1) >= (ccm_param.t_gui.target_nozzle_temp[1] - 3);
    }

    if (is_heating_done)   //最后几度要等待挺长时间的，不再等待
    {
      if (!timeOutFlag)
      {
        USER_EchoLogStr("M701 start load\r\n");//串口上传信息到上位机2017.7.6

        if (P2_Pro_NEW == ccm_param.t_sys_data_current.model_id || P3_Pro == ccm_param.t_sys_data_current.model_id)
        {
          if (flash_param_t.extruder_type == EXTRUDER_TYPE_MIX)
          {
            timeOutTickCount = xTaskGetTickCount() + 80 * 1000UL; //120s
          }
          else
          {
            timeOutTickCount = xTaskGetTickCount() + 80 * 1000UL; //120s
          }
        }
        else
        {
          timeOutTickCount = xTaskGetTickCount() + 120 * 1000UL; //120s
        }

        timeOutFlag = 1;
      }
    }

    if (timeOutFlag && !is_process_load_unload_done)
    {
      bool is_above_min_temp = (int)sg_grbl::temperature_get_extruder_current(0) >= ccm_param.motion_3d.extrude_min_temp;

      if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL)
      {
        is_above_min_temp = is_above_min_temp && (int)sg_grbl::temperature_get_extruder_current(1) >= ccm_param.motion_3d.extrude_min_temp;
      }

      if (is_above_min_temp)
      {
        if (sg_grbl::planner_moves_planned() > 1)   //只有1个有效block的时候，继续发送进丝命令
        {
          return;
        }
        else
        {
          user_send_internal_cmd((char *)"G92 E0 B0");

          // 消除EB高度差
          if (is_load_diff_eb && (P2_Pro_NEW == ccm_param.t_sys_data_current.model_id || P3_Pro == ccm_param.t_sys_data_current.model_id))
          {
            if (flash_param_t.extruder_type == EXTRUDER_TYPE_MIX)
            {
              user_send_internal_cmd((char *)"G1 F6000 E40 B85");
              user_send_internal_cmd((char *)"G92 E0 B0");
            }

            is_load_diff_eb = false;
          }

          if (1 == ccm_param.t_sys_data_current.enable_color_mixing)
          {
            user_send_internal_cmd((char *)"G1 F140 E15 B15"); //2017.4.24更改为140，增大挤出头拉力
          }
          else
          {
            user_send_internal_cmd((char *)"G1 F140 E10 B10"); //2017.4.24更改为140，增大挤出头拉力
          }
        }
      }
    }

    #elif defined(STM32F407xx)

    if ((int)sg_grbl::temperature_get_extruder_current(0) >= (ccm_param.t_gui.target_nozzle_temp[0] - 3))   //最后几度要等待挺长时间的，不再等待
    {
      if (!timeOutFlag)
      {
        if (K5 == ccm_param.t_sys_data_current.model_id)
        {
          if (1 == ccm_param.t_sys_data_current.enable_color_mixing)
          {
            timeOutTickCount = xTaskGetTickCount() + (60 * 1000UL); //120s
          }
          else
          {
            timeOutTickCount = xTaskGetTickCount() + (90 * 1000UL); //120s
          }
        }
        else
        {
          timeOutTickCount = xTaskGetTickCount() + (120 * 1000UL); //120s
        }

        timeOutFlag = 1;
      }
    }

    if (timeOutFlag && (!is_process_load_unload_done))
    {
      if ((int)sg_grbl::temperature_get_extruder_current(0) >= ccm_param.motion_3d.extrude_min_temp)
      {
        if (sg_grbl::planner_moves_planned() > 1)   //只有1个有效block的时候，继续发送进丝命令
        {
          return;
        }
        else
        {
          if (m83_process)
          {
            user_send_internal_cmd("M83 isInternal"); // 设置喷嘴E、B为绝对模式
            m83_process = 0;
          }

          if (M14S == ccm_param.t_sys_data_current.model_id)
          {
            user_send_internal_cmd("G1 F80 E5 B5 isInternal");
          }
          else
          {
            if (1 == ccm_param.t_sys_data_current.enable_color_mixing)
            {
              if (K5 == ccm_param.t_sys_data_current.model_id)
              {
                user_send_internal_cmd("G1 F300 E20 B20 isInternal");//2017.4.24更改为140，增大挤出头拉力
                //              user_send_internal_cmd("G1 F300 E3 B3 isInternal");//2017.4.24更改为140，增大挤出头拉力
                //              user_send_internal_cmd("G1 F400 E2 B2 isInternal");//2017.4.24更改为140，增大挤出头拉力
                //              user_send_internal_cmd("G1 F300 E3 B3 isInternal");//2017.4.24更改为140，增大挤出头拉力
                //              user_send_internal_cmd("G1 F400 E2 B2 isInternal");//2017.4.24更改为140，增大挤出头拉力
              }
              else
              {
                user_send_internal_cmd("G1 F140 E15 B15 isInternal");//2017.4.24更改为140，增大挤出头拉力
              }
            }
            else
            {
              user_send_internal_cmd("G1 F140 E10 B10 isInternal");//2017.4.24更改为140，增大挤出头拉力
            }
          }
        }
      }
    }

    #endif
  }


  // 执行退丝操作
  static void _control_process_unload(void)
  {
    #if defined(STM32F429xx)
    static int filament_mm_count = 0;
    timeOutTickCount = xTaskGetTickCount() + 60 * 1000UL; //60s
    bool is_heating_done = (int)sg_grbl::temperature_get_extruder_current(0) >= (ccm_param.t_gui.target_nozzle_temp[0] - 3);

    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL)
    {
      is_heating_done = is_heating_done && (int)sg_grbl::temperature_get_extruder_current(1) >= (ccm_param.t_gui.target_nozzle_temp[1] - 3);
    }

    //if((int)degHotend(0)>=(FilamentTemp-3))  //最后几度要等待挺长时间的，不再等待
    if (is_heating_done)   //最后几度要等待挺长时间的，不再等待
    {
      if (!timeOutFlag)
      {
        USER_EchoLogStr("M702 start unload\r\n");//串口上传信息到上位机2017.7.6
        timeOutFlag = 1;
        user_send_internal_cmd((char *)"G92 E0 B0");
        user_send_internal_cmd((char *)"G1 F200 E30 B30");

        if (1 == ccm_param.t_sys_data_current.enable_color_mixing)
        {
          if (P2_Pro_NEW == ccm_param.t_sys_data_current.model_id || P3_Pro == ccm_param.t_sys_data_current.model_id)
          {
            if (flash_param_t.extruder_type == EXTRUDER_TYPE_MIX)
            {
              user_send_internal_cmd((char *)"G92 E0 B0");
              user_send_internal_cmd((char *)"G1 F8400 E-70 B-70");
              user_send_internal_cmd((char *)"G92 E0 B0");
              user_send_internal_cmd((char *)"G1 F2500 E-70 B-70");
            }
            else if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
            {
              user_send_internal_cmd((char *)"G92 E0 B0");
              user_send_internal_cmd((char *)"G1 F8400 E-15 B-15");
              user_send_internal_cmd((char *)"G92 E0 B0");
              user_send_internal_cmd((char *)"G1 F2500 E-70 B-70");
            }
          }
          else
          {
            user_send_internal_cmd((char *)"G92 E0 B0");
            user_send_internal_cmd((char *)"G1 F8400 E-15 B-5"); //2017425退丝刚开始，快速退丝；B电机离喷嘴近点测试得退5mm以内最合适，E电机离喷嘴较远，退多一点
            user_send_internal_cmd((char *)"G92 E0 B0");
            user_send_internal_cmd((char *)"G1 F2500 E-65 B-70");
          }
        }

        filament_mm_count = 80;
      }
    }

    if (timeOutFlag && !is_process_load_unload_done)
    {
      bool is_above_min_temp = (int)sg_grbl::temperature_get_extruder_current(0) >= ccm_param.motion_3d.extrude_min_temp;

      if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL)
      {
        is_above_min_temp = is_above_min_temp && (int)sg_grbl::temperature_get_extruder_current(1) >= ccm_param.motion_3d.extrude_min_temp;
      }

      //if((int)degHotend(0)>=(FilamentTemp-30))
      if (is_above_min_temp)
      {
        if (sg_grbl::planner_moves_planned() > 1)
        {
          return;
        }
        else     //只有1个有效block的时候，继续发送退丝命令
        {
          user_send_internal_cmd((char *)"G1 F500 E-10 B-10");
          filament_mm_count += 10;

          if (filament_mm_count > 400)
          {
            timeOutTickCount = 0;
          }
        }
      }
    }

    #elif defined(STM32F407xx)
    static int filament_mm_count = 0;
    timeOutTickCount = xTaskGetTickCount() + (60 * 1000UL); //60s

    //if((int)degHotend(0)>=(FilamentTemp-3))  //最后几度要等待挺长时间的，不再等待
    if ((int16_t)sg_grbl::temperature_get_extruder_current(0) >= (ccm_param.t_gui.target_nozzle_temp[0] - 3))   //最后几度要等待挺长时间的，不再等待
    {
      if (!timeOutFlag)
      {
        timeOutFlag = 1;

        if (m83_process)
        {
          user_send_internal_cmd("M83 isInternal"); // 设置喷嘴E、B为绝对模式
          m83_process = 0;
        }

        if (M14S == ccm_param.t_sys_data_current.model_id)
        {
          user_send_internal_cmd("G92 E0 B0 isInternal");
          user_send_internal_cmd("G1 F80 E15 B15 isInternal");
          user_send_internal_cmd("G92 E0 B0 isInternal");
          user_send_internal_cmd("G1 F80 E-80 B-80 isInternal");
        }
        else
        {
          user_send_internal_cmd("G92 E0 B0 isInternal");

          if (ccm_param.t_sys_data_current.enable_color_mixing)
          {
            if (K5 == ccm_param.t_sys_data_current.model_id)
            {
              user_send_internal_cmd("G1 F300 E50 B50 isInternal");//2017.4.24更改为140，增大挤出头拉力
              user_send_internal_cmd("G92 E0 B0 isInternal");
              user_send_internal_cmd("G1 F1800 E-40 B-30 isInternal");//2017425退丝刚开始，快速退丝；B电机离喷嘴近点测试得退5mm以内最合适，E电机离喷嘴较远，退多一点
              user_send_internal_cmd("G92 E0 B0 isInternal");
              user_send_internal_cmd("G1 F800 E-10 B-10 isInternal");
              user_send_internal_cmd("G92 E0 B0 isInternal");
            }
            else
            {
              user_send_internal_cmd("G1 F200 E50 B50 isInternal");
              user_send_internal_cmd("G92 E0 B0 isInternal");
              user_send_internal_cmd("G1 F8400 E-15 B-5 isInternal");//2017425退丝刚开始，快速退丝；B电机离喷嘴近点测试得退5mm以内最合适，E电机离喷嘴较远，退多一点
              user_send_internal_cmd("G92 E0 B0 isInternal");
              user_send_internal_cmd("G1 F2500 E-65 B-70 isInternal");
            }
          }
          else
          {
            user_send_internal_cmd("G1 F200 E50 B50 isInternal");
          }
        }

        filament_mm_count = 80;
      }
    }

    if (timeOutFlag && (!is_process_load_unload_done))
    {
      //if((int)degHotend(0)>=(FilamentTemp-30))
      if ((int)sg_grbl::temperature_get_extruder_current(0) >= ccm_param.motion_3d.extrude_min_temp/*(ccm_param.t_gui.target_nozzle_temp[0] - 30)*/)
      {
        if (sg_grbl::planner_moves_planned() > 1)
        {
          return;
        }
        else     //只有1个有效block的时候，继续发送退丝命令
        {
          if (M14S == ccm_param.t_sys_data_current.model_id)
          {
            user_send_internal_cmd("G1 F80 E-5 B-5 isInternal");
          }
          else
          {
            user_send_internal_cmd("G1 F300 E-10 B-10 isInternal");
          }

          filament_mm_count += 10;

          if (filament_mm_count > 400)
          {
            timeOutTickCount = 0;
          }
        }
      }
    }

    #endif
  }

  void control_init()
  {
    startLoadFlag = 0;
    startUnloadFlag = 0;
    timeOutFlag = 0;
    timeOutTickCount = 0;
  }

  // 开始进丝
  void control_start_load(void)
  {
    #if defined(STM32F429xx)

    if (!startLoadFlag) // 避免重复触发
      _control_prepare();

    startLoadFlag = 1;
    is_load_diff_eb = true;
    #elif defined(STM32F407xx)

    if (!startLoadFlag) // 避免重复触发
    {
      Lowtemp_load = true;
      _control_prepare();
    }

    m83_process = 1;
    startLoadFlag = 1;
    #endif
  }

  // 开始退丝
  void control_start_unload(void)
  {
    #if defined(STM32F429xx)

    if (!startUnloadFlag) // 避免重复触发
      _control_prepare();

    startUnloadFlag = 1;
    #elif defined(STM32F407xx)

    if (!startUnloadFlag) // 避免重复触发
    {
      Lowtemp_load = false;
      _control_prepare();
    }

    m83_process = 1;
    startUnloadFlag = 1;
    #endif
  }

  // 执行进退丝入口
  void control_process(void)
  {
    if ((!startLoadFlag) && (!startUnloadFlag)) // 进退丝标志位都为false，退出
    {
      return;
    }

    if (startLoadFlag)
    {
      _control_process_load();
    }

    if (startUnloadFlag)
    {
      _control_process_unload();
    }

    //进丝成功或退丝成功
    if (timeOutFlag)
    {
      // 判断进丝或退丝加热是否完成
      ccm_param.t_gui_p.IsFinishedFilamentHeat = 1;

      if (xTaskGetTickCount() > timeOutTickCount)
      {
        is_process_load_unload_done = true;
      }

      if (is_process_load_unload_done && sg_grbl::planner_moves_planned() == 0)
      {
        // 退出进退丝操作
        _control_exit(false);
        // 判断进丝或退丝是否完成
        ccm_param.t_gui_p.IsSuccessFilament = 1;
      }
    }

    osDelay(50);
  }

  void control_cancel_process(void)
  {
    stepper_quick_stop(); // 电机快速停止
    _control_exit(true);
  }

}


#endif











