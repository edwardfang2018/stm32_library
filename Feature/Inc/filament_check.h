#ifndef FILAMENT_CHECK_H
#define FILAMENT_CHECK_H

#ifdef HAS_FILAMENT

#ifdef __cplusplus
extern "C" {
#endif
namespace feature_filament
{
  extern void check_init(void);
  extern void check(void);
}
#ifdef __cplusplus
} //extern "C"
#endif

#endif

#endif // FILAMENT_CHECK_H

