#ifndef SYSCONFIG_H
#define SYSCONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
} //extern "C"
#endif

class SysConfigOperation
{
public:
  SysConfigOperation();
  void readInfo(void);
  void ChangeBootVersionInfo(void);
  void ChangeModelInfo(void);
  void ChangePictureInfo(void);
  void ChangeFunctionInfo(void);
  void ChangePidOutputFactorValue(void);
  void ChangelogoInfo(void);
  void ChangeZOffsetZeroValue(void);

  void ChangeLaser(void);
  void ChangeIsSoftfilament(void);
  //  void ChangeLOGOinterface(void);
  void ChangeLogoID(void);
  void ChangeCustomModelId(void);

  void saveInfo(bool isFunction);
private:
  /*save sysconfig info Function*/
  void changeMachineSettingMark(void);
};

class SysConfig
{
public:
  SysConfig();
  void init(void);

private:
  /*get sysconfig info Function*/
  void explainInfo(void);
  void getModelStr(void);
  void getStatusInfoStr(void);

};

#endif // SYSCONFIG_H

