#include "user_common_cpp.h"
#ifdef ENABLE_FLASH

#include "ConfigurationStore.h"

#ifdef __cplusplus
extern "C" {
#endif

void flash_interface_process(void)
{
  static uint32_t flash_save_timeout = 0;

  if (flash_save_timeout < xTaskGetTickCount())
  {
    flash_save_timeout = xTaskGetTickCount() + 1000;

    if (flash_param_t.flag == 1)
    {
      taskENTER_CRITICAL();
      flash_param_t.flag = 0;
      flash_config_save();
      taskEXIT_CRITICAL();
    }
    else if (flash_poweroff_data_is_reset)
    {
      taskENTER_CRITICAL();
      flash_poweroff_data_is_reset = false;
      flash_poweroff_data_erase();
      taskEXIT_CRITICAL();
    }
  }
}


#ifdef __cplusplus
}
#endif

#endif







