#include "user_common_cpp.h"

#ifdef HAS_FILAMENT

#if defined(STM32F429xx)

#include "gcode.h"
#include "process_m_code.h"
#include "temperature.h"
#include "stepper.h"
#include "commonf.h"
#include "common.h"
static uint8_t filament_check_count = 0;
static uint32_t filament_check_timeout = 0;

namespace feature_filament
{

  static void _check_err(void)
  {
    if (filament_check_count < 5)
    {
      filament_check_count++;
    }
  }

  static void _reset_check_count(void)
  {
    filament_check_count = 0;
    transFilePrintStatus.isFilamentCheck = false;
  }

  static void _check_idex_axis(const int _active_extruder, const int _change_extruder)
  {
    if (1 == ccm_param.t_sys.idex_print_type_single_mode) // 非双色模型
    {
      USER_EchoLogStr("feature_filament_check ==>> (idex_single) E%d not filament!\r\n", _active_extruder);
      _reset_check_count();
      ccm_param.t_gui_p.m109_heating_complete = false; //设置为未加热
      USER_SEND_INTERNAL_CMD_BUF("M400"); //等待指令执行完成
      USER_SEND_INTERNAL_CMD_BUF("T%d", _change_extruder); //切换喷头
      USER_SEND_INTERNAL_CMD_BUF("M104 T%d S50", _active_extruder); //降温当前喷头
      USER_SEND_INTERNAL_CMD_BUF("M109 T%d S%d", _change_extruder, (int)sg_grbl::temperature_get_extruder_target(_active_extruder)); //设置切换喷头温度
      USER_SEND_INTERNAL_CMD_BUF("G92 E%f", ccm_param.grbl_destination[_active_extruder == 0 ? E_AXIS : B_AXIS]); //设置切换头E值
      USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.grbl_destination[Z_AXIS] + 10.0f);
      float x = 0.0f;

      if (_active_extruder == 0)
      {
        x =  ccm_param.grbl_destination[X_AXIS] - flash_param_t.idex_extruder_0_bed_offset[0];

        if (x >= ccm_param.motion_3d_model.xyz_min_pos[X_AXIS])
        {
          USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f", x, ccm_param.grbl_destination[Y_AXIS]);
        }
      }
      else if (_active_extruder == 1)
      {
        x =  ccm_param.grbl_destination[X2_AXIS] - (flash_param_t.idex_extruder_0_bed_offset[0] - flash_param_t.idex_extruder_1_bed_offset[0]);

        if (x >= ccm_param.motion_3d_model.xyz_min_pos[X2_AXIS])
        {
          USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f", x, ccm_param.grbl_destination[Y_AXIS]);
        }
      }

      USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.grbl_destination[Z_AXIS]);
    }
    else
    {
      USER_EchoLogStr("feature_filament_check ==>> (idex_dual) E%d not filament!\r\n", _active_extruder);
      _check_err();
    }
  }

  static void _check_idex(void)
  {
    if (user_pin_sig_mat_e0_read() && user_pin_sig_mat_e1_read())
    {
      USER_EchoLogStr("feature_filament_check ==>> (dual) E0 and E1 not filament!\r\n");
      _check_err();
    }
    else
    {
      if (gcode::active_extruder == 0)
      {
        if (user_pin_sig_mat_e0_read())
        {
          _check_idex_axis(0, 1);
        }
        else
        {
          _reset_check_count();
        }
      }
      else if (gcode::active_extruder == 1)
      {
        if (user_pin_sig_mat_e1_read())
        {
          _check_idex_axis(1, 0);
        }
        else
        {
          _reset_check_count();
        }
      }
    }
  }

  static void _check_single(void)
  {
    if (user_pin_sig_mat_e0_read())
    {
      _check_err();
    }
    else
    {
      _reset_check_count();
    }
  }

  void check_init(void)
  {
  }

  void check(void)
  {
    if (ccm_param.t_sys_data_current.enable_material_check) //有断料检测功能
    {
      if (filament_check_timeout < xTaskGetTickCount())
      {
        if (print_status.is_printing && ccm_param.t_gui_p.m109_heating_complete) //打印开始且加热完成后 才去检测是否有料
        {
          if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1) // idex模式
          {
            if (ccm_param.t_sys.idex_print_type == IDEX_PRINT_TYPE_NORMAL) //正常模式
            {
              _check_idex(); // idex
            }
            else if (ccm_param.t_sys.idex_print_type == IDEX_PRINT_TYPE_COPY || ccm_param.t_sys.idex_print_type == IDEX_PRINT_TYPE_MIRROR) // 克隆、镜像模式
            {
              if (user_pin_sig_mat_e0_read() || user_pin_sig_mat_e1_read()) // 任意头没料，报警
              {
                USER_EchoLogStr("feature_filament_check ==>> (copy\\mirror) E0 or E1 not filament!\r\n");
                _check_err();
              }
            }
          }
          else if (flash_param_t.extruder_type == EXTRUDER_TYPE_LASER || flash_param_t.extruder_type == EXTRUDER_TYPE_DRUG) // 激光模式不检测
          {
            return;
          }
          else
          {
            _check_single(); // 单头、混色头
          }
        }

        if (filament_check_count == 3)
        {
          filament_check_count = 0;
          USER_EchoLogStr("IsNotHaveMatInPrint\r\n");
          transFilePrintStatus.isFilamentCheck = true;
          ccm_param.t_gui_p.IsNotHaveMatInPrint = 1; //在打印的时候没料了
          osDelay(100);
#ifndef ENABLE_GUI_LVGL
          waiting_for_pausing(NoHaveMatWaringInterface);
#endif
        }

        filament_check_timeout = MILLIS() + 1000;
      }
    }
  }
}
#elif defined(STM32F407xx)
#include "Alter.h"
#include "user_interface.h"
#include "planner.h"
#include "view_commonf.h"
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////// MaterialCheck    start         ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

namespace feature_filament
{
  static void Check_NotHaveMat(void)
  {
    static int8_t nomat_cnt = 0;
    static uint32_t NotHaveMatTimeOut = 0;

    if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5))
    {
      sys_os_delay(10);//防抖

      if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5))
      {
        if (NotHaveMatTimeOut < sys_task_get_tick_count())
        {
          ++nomat_cnt;
          NotHaveMatTimeOut = sys_task_get_tick_count() + 3000;
        }

        if (nomat_cnt > 3)
        {
          USER_EchoLogStr("IsNotHaveMatInPrint\r\n");
          ccm_param.t_gui_p.IsNotHaveMatInPrint = 1; //在打印的时候没料了
          osDelay(100);
          gui_waiting_for_pausing();
          nomat_cnt = 0;
        }
      }
    }
    else
    {
      nomat_cnt = 0;
    }
  }

  void check_init(void)
  {
    if (ccm_param.t_sys_data_current.enable_material_check) //有断料检测功能
    {
      if (ccm_param.t_sys_data_current.model_id == M41G)
      {
        //修改M41G 5V_FAN做断料检测IO
        GPIO_InitTypeDef GPIO_InitStruct;
        GPIO_InitStruct.Pin = GPIO_PIN_14;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
      }
      else if (ccm_param.t_sys_data_current.model_id == M3145S && (1 == ccm_param.t_sys_data_current.enable_cavity_temp))
      {
        //修改M3145S 开门检查做断料检测IO
        GPIO_InitTypeDef GPIO_InitStruct;
        GPIO_InitStruct.Pin = GPIO_PIN_15;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
      }
      else if (ccm_param.t_sys_data_current.model_id == K5)
      {
        GPIO_InitTypeDef GPIO_InitStruct;
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5);
        GPIO_InitStruct.Pin = GPIO_PIN_5;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
      }
      else
      {
        GPIO_InitTypeDef GPIO_InitStruct;
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5);
        GPIO_InitStruct.Pin = GPIO_PIN_5;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
      }
    }
  }

  void check(void)
  {
    if (ccm_param.t_sys_data_current.enable_material_check) //有断料检测功能
    {
      if (ccm_param.t_gui_p.IsNotHaveMatInPrint == 1) return;

      if (print_status.is_printing && (1U == ccm_param.t_gui_p.m109_heating_complete) && (1U == ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE)) //打印开始且加热完成归零后 才去检测是否有料
      {
        if (ccm_param.t_sys_data_current.model_id == M41G)
        {
          if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_14))
          {
            sys_os_delay(10);//防抖

            if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_14))
            {
              USER_EchoLogStr("IsNotHaveMatInPrint\r\n");
              ccm_param.t_gui_p.IsNotHaveMatInPrint = 1; //在打印的时候没料了
              osDelay(100);
              gui_waiting_for_pausing();
            }
          }
        }
        else if (ccm_param.t_sys_data_current.model_id == M3145S && (1 == ccm_param.t_sys_data_current.enable_cavity_temp))
        {
          if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15))
          {
            sys_os_delay(10);//防抖

            if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15))
            {
              USER_EchoLogStr("IsNotHaveMatInPrint\r\n");
              ccm_param.t_gui_p.IsNotHaveMatInPrint = 1; //在打印的时候没料了
              osDelay(100);
              gui_waiting_for_pausing();
            }
          }
        }
        else if (ccm_param.t_sys_data_current.model_id == K5)
        {
          Check_NotHaveMat();
        }
        else
        {
          if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5))
          {
            sys_os_delay(10);//防抖

            if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5))
            {
              USER_EchoLogStr("IsNotHaveMatInPrint\r\n");
              ccm_param.t_gui_p.IsNotHaveMatInPrint = 1; //在打印的时候没料了
              osDelay(100);
              gui_waiting_for_pausing();
            }
          }
        }
      }
    }
  }
}
#endif

#endif // HAS_FILAMENT_SENSOR

