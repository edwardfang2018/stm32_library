#ifndef USER_CUSTOM_DATA_H
#define USER_CUSTOM_DATA_H

#define F_CPU 16000000UL

#define HIGH 0x1
#define LOW  0x0

#ifndef min
  #define min(a,b) ((a)<(b)?(a):(b))
#endif // min

#ifndef max
  #define max(a,b) ((a)>(b)?(a):(b))
#endif // max

#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))

#if defined(STM32F407xx)
#define MAX_NUM_AXIS 5
#define XYZ_NUM_AXIS 3
enum AxisEnum {X_AXIS = 0, Y_AXIS = 1, Z_AXIS = 2, E_AXIS = 3, B_AXIS = 4};
#elif defined(STM32F429xx)
#define MAX_NUM_AXIS 7
#define XYZ_NUM_AXIS 5
enum AxisEnum {X_AXIS = 0, X2_AXIS = 1, Y_AXIS  = 2, Z_AXIS = 3, Z2_AXIS = 4, E_AXIS = 5, B_AXIS = 6};
#endif

extern const char axis_codes[MAX_NUM_AXIS];

//===========================================================================
//=============================Buffers           ============================
//===========================================================================

// The number of linear motions that can be in the plan at any give time.
// THE BLOCK_BUFFER_SIZE NEEDS TO BE A POWER OF 2, i.g. 8,16,32 because shifts and ors are used to do the ring-buffering.
#define BLOCK_BUFFER_SIZE 16 // maximize block buffer

#define  FORCE_INLINE  __attribute__((always_inline)) inline

#define MILLIS() xTaskGetTickCount()
#define TASK_ENTER_CRITICAL()  taskENTER_CRITICAL();
#define TASK_EXIT_CRITICAL()  taskEXIT_CRITICAL();
#define OS_DELAY(TICK_VALUE) osDelay(TICK_VALUE);

// 機型表
extern const short model_id_table[];
extern const float   model_size_table[][XYZ_NUM_AXIS];
extern const char *model_name_table[];


// 機型ID
enum ModelId
{
  M14 = 0,
  M2030 = 1,
  M2041 = 2,
  M2048 = 3,
  M3145 = 4,
  M4141 = 5,
  M4040 = 6, // 日本专用
  M4141S = 7,
  AMP410W = 8,
  M14R03 = 9,
  M2030HY = 10,
  M14S = 11,
  M3145S = 12,
  M15 = 13,
  M3036 = 14,
  M4141S_NEW = 15, //M4141S_NEW M41S改版
  M41G = 16,
  M3145T = 17,
  M3145K = 18,
  K5 = 19,
  #if defined(STM32F407xx)
  K600 = 20,
  K700 = 21,
  Test = 22,
  MODEL_COUNT = 23 // 机型数量
  #elif defined(STM32F429xx)
  F400TP = 20,
  F1000TP = 21,
  P2_Pro = 22,
  P3_Pro = 23,
  Drug = 24,
  F300TP = 25,
  P2_Pro_NEW = 26,
  K600 = 27

  #endif
};

#if defined(STM32F407xx)
typedef struct
{
  int16_t axis_num;                              // 機型軸數
  unsigned char step;                            // 機型細分數 1:16;2:32;
  unsigned char enable_poweroff_up_down_min_min; // 是否開啓上下共限位
  unsigned char enable_board_test;               // 是否開啓大板測試
  unsigned char enable_check_door_open;          // 是否有门检测
  unsigned char disable_z_max_limit;             // 是否有Z轴下限位开关
  unsigned char updown_g28_first_time;           // 上下限位公用限位開關，執行G28指令判斷Z零點位置
  unsigned char is_open_infrared_z_min_check;
  bool is_cal_bed_platform;
  float extrude_min_temp;
} motion_3d_t;

typedef struct
{
  float xyz_max_pos[3];                // XYZ最大位置
  float xyz_min_pos[3];                // XYZ最小位置
  float xyz_home_pos[3];               // XYZ零點位置
  float xyz_max_length[3];             // XYZ最大長度
  float xyz_home_retract_mm[3];        // XYZ歸零回抽距離mm
  float xyz_move_max_pos[3];           // XYZ最大移动位置
  float extrude_maxlength;             // prevent extrusion of very large distances.
  float z_max_pos_origin;              // 保存机型默认Z最大点，用于校准Z高度
  unsigned char enable_invert_dir[6];  // 是否反轉軸方向
  signed char xyz_home_dir[3];         // XYZ方向
} motion_3d_model_t;

#elif defined(STM32F429xx)
typedef struct
{
  unsigned char axis_num;                        // 機型軸數
  unsigned char step;                            // 機型細分數 1:16;2:32;
  #if defined(STM32F407xx)
  unsigned char enable_poweroff_up_down_min_min; // 是否開啓上下共限位
  #endif
  unsigned char enable_board_test;               // 是否開啓大板測試
  unsigned char enable_check_door_open;          // 是否有门检测
  unsigned char disable_z_max_limit;             // 是否有Z轴下限位开关
  #if defined(STM32F407xx)
  unsigned char updown_g28_first_time;           // 上下限位公用限位開關，執行G28指令判斷Z零點位置
  #endif
  unsigned char is_open_infrared_z_min_check;
  bool is_cal_bed_platform;
  float extrude_min_temp;
} motion_3d_t;

typedef struct
{
  float xyz_max_pos[MAX_NUM_AXIS];                // XYZ最大位置
  float xyz_min_pos[MAX_NUM_AXIS];                // XYZ最小位置
  float xyz_home_pos[MAX_NUM_AXIS];               // XYZ零點位置
  float xyz_max_length[MAX_NUM_AXIS];             // XYZ最大長度
  float xyz_home_retract_mm[MAX_NUM_AXIS];        // XYZ歸零回抽距離mm
  float xyz_move_max_pos[MAX_NUM_AXIS];           // XYZ最大移动位置
  float extrude_maxlength;             // prevent extrusion of very large distances.
  float z_max_pos_origin;              // 保存机型默认Z最大点，用于校准Z高度
  unsigned char enable_invert_dir[MAX_NUM_AXIS];  // 是否反轉軸方向
  signed char xyz_home_dir[MAX_NUM_AXIS];         // XYZ方向
} motion_3d_model_t;
#endif
#define LCD_TYPE_35_480320_SIZE 0 // 3.5寸 480*320
#define LCD_TYPE_43_480272_SIZE 1 // 4.3寸 480*272
#define LCD_TYPE_7_1024600_SIZE 2 // 7寸 1024*600

#define BED_LEVEL_PRESSURE_SENSOR 3 //红外激光头

#define EXTRUDER_TYPE_NONE 0
#define EXTRUDER_TYPE_SINGLE 1
#define EXTRUDER_TYPE_MIX 2
#define EXTRUDER_TYPE_DUAL 3
#define EXTRUDER_TYPE_LASER 4
#define EXTRUDER_TYPE_DRUG 5

#define IDEX_PRINT_TYPE_NORMAL 0
#define IDEX_PRINT_TYPE_COPY 1
#define IDEX_PRINT_TYPE_MIRROR 2

#define MIX_PRINT_TYPE_GRADIENT_COLOR 0
#define MIX_PRINT_TYPE_FIX_PROPORTION 1
#define MIX_PRINT_TYPE_RANDOM 2
///////////////////////////////////// SysConfig Start///////////////////////////////////////////

extern char sys_data[512];                      // sysconfig數據緩存
extern uint32_t sys_data_size;                  // sysconfig數據緩存大小

///////////////////////////////////// SysConfig End///////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 默认配置
#define DEFAULT_MODEL               0 // 默认配置//0:M14 1:M2030 2:M2041 3:M2048 4:M3145 5:M4141 6:M4040 7:M4141S 8:AMP410W 9:M14R03 10:M2030HY 11:M14S 12:M3145S    //4141S机型的X轴方向不一样
#define DEFAULT_COLORMIXING         0 // 1:打开混色功能 0:关闭混色功能
#define DEFAULT_POWEROFFRECOVERY    0 // 1:打开断电续打功能 0:关闭断电续打功能
#define DEFAULT_MATCHECK            0 // 1:打开断料检测功能 0：关闭断料检测功能
#define DEFAULT_STEP                1 // 1: 电机16细分 2： 电机32细分
#define DEFAULT_NUMAXIS             4 // 4: 基础版4个电机 5：混色版5个电机

// GUI圖片ID
#define PICTURE_IS_CHINESE  1
#define PICTURE_IS_JAPANESE 2
#define PICTURE_IS_ENGLISH  3
#define PICTURE_IS_KOREA    4
#define PICTURE_IS_RUSSIA   5
#define PICTURE_IS_CHINESE_TRADITIONAL   6

// 4.3寸屏UI
#define LCD_43_UI_TYPE_NONE 0
#define LCD_43_UI_TYPE_BLUE 1
#define LCD_43_UI_TYPE_BLACK 2

extern void sys_write_info(const TCHAR *filePath);
extern void sys_read_info(const TCHAR *filePath);

#endif // USER_CUSTOM_DATA_H

