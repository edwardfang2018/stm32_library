#ifndef USER_COMMON_CPP_H
#define USER_COMMON_CPP_H

#include "./user_common.h"
#include "../../Feature/Inc/control_interface.h"
#include "../../Feature/Inc/bed_level_mechanical.h"

#ifdef ENABLE_FLASH
  #include "../../Feature/Inc/flash_config.h"
  #include "../../Feature/Inc/flash_poweroff_data.h"
#endif

#ifdef HAS_FILAMENT
  #include "../../Feature/Inc/filament_check.h"
  #include "../../Feature/Inc/filament_mid_chg.h"
  #include "../../Feature/Inc/filament_control.h"
#endif

#ifdef HAS_PRINT_CONTROL
  #include "../../Feature/Inc/print_control.h"
#endif

#ifdef HAS_FAN_CONTROL
  #include "../../Feature/Inc/control_fan.h"
#endif

#ifdef HAS_LED_CONTROL
  #include "../../Feature/Inc/control_led.h"
#endif

#ifdef HAS_DOOR_CONTROL
  #include "../../Feature/Inc/control_door.h"
#endif

#ifdef HAS_BOARD_TEST
  #include "../../Feature/Inc/board_test.h"
#endif

#endif // USER_COMMON_CPP_H



















