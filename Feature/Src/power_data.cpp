#include "user_common_cpp.h"

#ifdef HAS_POWER_RECOVERY
#include <cstring>
#include "temperature.h"
#include "stepper.h"
#if defined(STM32F429xx)
  #include "planner.h"
  #include "process_command.h"
#elif defined(STM32F407xx)
  #include "gcode_global_params.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

void feature_pow_data_init(void)
{
  if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
  {
    flash_poweroff_data_read();

    if (0U != ccm_param.flash_poweroff_recovery.is_serial) // 串口标志位开，断电标志关闭
    {
      ccm_param.flash_poweroff_recovery.flag = 0U;
    }

    memcpy((planner_running_status_t *)&ccm_param.t_power_off,
            (planner_running_status_t *)&ccm_param.flash_poweroff_recovery, sizeof(planner_running_status_t));
    utils_str_memmove(ccm_param.power_path_file_name, flash_param_t.pathFileName);
    utils_str_memmove(ccm_param.power_file_name, flash_param_t.fileName);
    ccm_param.t_sys.enable_color_buf = ccm_param.flash_poweroff_recovery.enable_color_buf;
    ccm_param.t_sys.print_time_save = ccm_param.flash_poweroff_recovery.print_time_save;

    if ((uint8_t)(flash_param_t.pathFileName[0] - '0') == 0)
    {
      #if defined(STM32F429xx)
      ccm_param.power_is_file_from_sd = 0;
      #elif defined(STM32F407xx)
      ccm_param.power_is_file_from_sd = 1;
      #endif
    }
    else
    {
      #if defined(STM32F429xx)
      ccm_param.power_is_file_from_sd = 1;
      #elif defined(STM32F407xx)
      ccm_param.power_is_file_from_sd = 0;
      #endif
    }
  }
}

// 重置断电标志位
void feature_pow_data_reset_flag(void)
{
  if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
  {
    taskENTER_CRITICAL();
    memset((planner_running_status_t *)&ccm_param.t_power_off, 0, sizeof(ccm_param.t_power_off));
    taskEXIT_CRITICAL();
  }
}

//断电续打取消删除SD卡中的文件
void feature_pow_data_delete_file_from_sd(void)
{
  if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
  {
    if (ccm_param.power_is_file_from_sd)   //断电续打文件在SD卡中，删除文件
    {
      taskENTER_CRITICAL();
      (void)f_unlink(ccm_param.power_path_file_name);
      taskEXIT_CRITICAL();
    }
  }
}

//断电续打取消删除SD卡中的文件
void feature_pow_data_save_file_path_name(TCHAR *path_file_name, TCHAR *file_name)
{
  if ((0U != ccm_param.t_sys_data_current.enable_powerOff_recovery)) // 断电开启，非断电状态
  {
    /*确定选择的打印文件后，将文件名和路径名保存到flash，以便断电续打时调用*/
    utils_str_memmove(flash_param_t.pathFileName, path_file_name);
    utils_str_memmove(flash_param_t.fileName, file_name);
    #if defined(STM32F429xx)
    flash_param_t.idex_print_type = ccm_param.t_sys.idex_print_type;
    flash_param_t.mix_print_type = ccm_param.t_sys.mix_print_type;
    #endif
  }
}

#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_POWER_RECOVERY













