#ifndef USER_CCM_H
#define USER_CCM_H

#include "planner_block_buffer.h"

#define PIC_BUF_SIZE          (1024*12) // 12k大小
#define MAX_CMD_SIZE          96        // 定义最大接收字节数 96
#define POWER_OFF_BUF_SIZE    512       //  数据长度不能超过512，否则溢出,以字符串形式存储，一次性写入到文件11
#if defined(STM32F429xx)
  #define BUFSIZE               1   /*!< 定义环形数组大小为 256 */
  #if defined(STM32F407xx)
    #define TEXT_BUF_24_12_9_SIZE (24*12*9)
    #define TEXT_BUF_24_12_3_SIZE (24*12*3)
  #elif defined(STM32F429xx)
    #ifndef ENABLE_GUI_LVGL
      #define TEXT_BUF_24_12_9_SIZE (24*12*9)
      #define TEXT_BUF_24_12_3_SIZE (24*12*3)
    #else
      #define TEXT_BUF_24_12_9_SIZE (2)
      #define TEXT_BUF_24_12_3_SIZE (2)
    #endif
  #endif
  #define FILE_READ_SIZE  512
#elif defined(STM32F407xx)
  #define TRANS_FILE_BUF_SIZE (2048)  //256
  #define BUFSIZE          32          /*!< 定义环形数组大小为 256 */
  #define FILE_READ_SIZE  1024
#endif
#ifdef __cplusplus
extern "C" {
#endif

#define CAL_Z_MAX_POS_OFFSET 20.0F                 // 校準Z最大位置時，校準值與原來MAX Z的偏移量
typedef struct
{
  uint8_t disable_abs;                          // 是否能打印ABS
  uint8_t disable_hot_bed;                      // 是否开启热床
  uint8_t enable_warning_light;                 // 是否有警示灯
  uint8_t enable_led_light;                     // 是否有LED照明
} T_CUSTOM_SERVICES;

typedef struct
{
  int32_t printed_time_sec;                         // 已打印的时间
  int32_t used_total_material;                      // 耗材总长度
  long machine_run_time;                            // 机器运行时间
  int16_t nozzle_temp[2];                           // 喷嘴温度
  int16_t target_nozzle_temp[2];                    // 喷嘴目标温度
  float move_xyz_pos[XYZ_NUM_AXIS];               // 移动XYZ轴
  #if defined(STM32F407xx)
  uint32_t file_position;                           // 打印文件当前指针位置
  #elif defined(STM32F429xx)
  uint8_t is_refresh_rtc;                           // 是否GUI刷新
  #endif
  int16_t hot_bed_temp;                             // 热床温度
  int16_t target_hot_bed_temp;                      // 热床目标温度
  uint16_t print_speed_value;                       // 打印速度
  uint16_t fan_speed_value;                         // 风扇速度
  uint16_t cavity_temp;                             //
  uint16_t target_cavity_temp;                      //
  uint16_t target_cavity_temp_on;                   //
  uint8_t print_percent;                            // 打印进度-百分数
  uint32_t printfile_size;                          // 打印文件总大小（字节）
  uint32_t file_size;                               // 打印文件剩余大小（字节）
  uint16_t cura_speed;                              // 获取到的cura软件上的速度，M117命令传送
} T_GUI;



typedef struct
{
  char boot_ver_str[20];                   // boot版本
  char app2_ver_str[20];                   // app2版本
  char build_date_str[20];                 // 制作时间
  unsigned char have_set_machine;          // 是否已經設置機器
  unsigned char model_id;                  // 要改变为哪种机型
  unsigned char pic_id;                    // 要改变为哪种图片 中文图片或日文图片
  unsigned char enable_powerOff_recovery;  // 是否有斷電功能
  float poweroff_rec_z_max_value;          // 斷電z最大高度
  unsigned char enable_color_mixing;       // 是否有混色功能
  unsigned char enable_material_check;     // 是否有断料检测功能
  float material_chk_vol_value;            //
  unsigned char enable_block_detect;       // 是否開啓堵料檢測
  float bed_level_z_at_left_front;         //
  float bed_level_z_at_right_front;        //
  float bed_level_z_at_left_back;          //
  float bed_level_z_at_right_back;         //
  float bed_level_z_at_middle;             //
  float pid_output_factor;                 // 加热系数
  unsigned char enable_bed_level;          // 是否开启自动调平
  unsigned char enable_soft_filament;      // 是否使用软料
  unsigned char enable_LOGO_interface;     // 是否开启开机LOGO界面
  unsigned char logo_id;                   // logo图片的id号
  unsigned char custom_model_id;           // 定制机型id
  unsigned char buzzer_value;              // 按键声、报警声开关
  float z_offset_value;                    // Z零点偏移
  unsigned char enable_v5_extruder;//25
  unsigned char enable_cavity_temp;//26
  unsigned char enable_type_of_thermistor;//27
  unsigned char enable_high_temp;//28
  unsigned char ui_number;//29
  unsigned char is_2GT;//30
  unsigned char IsMechanismLevel;//31
  unsigned char IsLaser;//32
  unsigned char IsLaserMode;//33
  unsigned char lcd_type;
  unsigned char cf35;
  unsigned char cf36;
  unsigned char cf37;
  unsigned char cf38;
  unsigned char cf39;
  unsigned char cf40;
} T_SYS_DATA;

typedef struct
{
  char model_str[30];                           // 机型字符串
  char function_str[35];                        // 功能字符串
  char version_str[30];                         // 版本字符串
  int key_sound;
  int alarm_sound;
  unsigned char is_bed_level_down_to_zero;     // 平台是否下归零
  unsigned char is_detect_extruder_thermistor; // 是否检测热敏电阻脱离
  unsigned char is_planner_slow_down;          // 是否planner减速
  unsigned short serial_moves_queued;          // 串口移动指令数
  unsigned char lcd_ssd1963_43_480_272;
  unsigned int print_time_save;
  unsigned char lcd_type;
  #if defined(STM32F407xx)
  unsigned char enable_automatic_refueling;    // 是否启动自动换料功能
  unsigned char is_granulator;
  unsigned int pulse_delay_time;
  uint8_t enable_color_buf;
  #elif defined(STM32F429xx)
  unsigned char enable_cavity_temp;            // 是否开启腔体温度开关
  unsigned char enable_color_buf;
  unsigned char is_idex_extruder;  //是否为idex结构
  unsigned char idex_print_type;  //是否为idex结构
  unsigned char idex_print_type_bak;  //是否为idex结构
  unsigned char idex_print_type_single_mode;  //是否为idex结构
  unsigned char mix_print_type;  //是否为idex结构
  unsigned char is_abl_init;  //是否初始化abl
  #endif
} T_SYS;

// 运行状态
typedef struct
{
  volatile float axis_position[MAX_NUM_AXIS];
  volatile float axis_real_position[MAX_NUM_AXIS];
  volatile float z_real_change_value;
  volatile float target_bed_temp;                                   /*!< 热床目标温度 */
  volatile float target_extruder0_temp;                             /*!< 喷嘴目标温度 */
  volatile float target_extruder1_temp;                             /*!< 喷嘴目标温度 */
  volatile float feed_rate;                                         /*!< 进料速度 */
  volatile uint32_t sd_position;                                    /*!< 文件位置 */
  volatile uint32_t print_time_save;                                /*!< 打印时间 */
  volatile int32_t extruder_multiply;                               /*!< 移动速度百分比 */
  volatile int32_t feed_multiply;                                   /*!< 进料速度百分比 */
  volatile int32_t fan_speed;                                       /*!< 风扇速度 */
  volatile int32_t layer_count;                                     /*!< 打印文件层数 */
  volatile int32_t current_layer;                                   /*!< 当前层数 */
  volatile uint8_t enable_color_buf;                                /*!< 是否混色 */
  volatile uint8_t active_extruder;                                 /*!< 当前活动喷头 */
  volatile uint8_t is_serial;                                       /*!< 串口标志位 */
  volatile uint8_t flag;                                            /*!< 断电标志位 */
} planner_running_status_t;

#define OnePageNum 4
typedef struct
{
  TCHAR CurrentPath[100];                  //当前目录的路径
  TCHAR DisplayFileName[OnePageNum][100];  //当前GUI要显示的文件名
  TCHAR SDFileName[50];  //电脑上传的文件名
  uint8_t IsNotHaveMatInPrint; //在打印的时候是否没料了
  uint8_t IsDisplayDoorOpenInfo; //是否显示门打开的提示信息
  uint8_t IsDoorOpen; //M14R03,M14S检测到门是否打开
  uint8_t doorStatus; //门状态
  uint8_t M600FilamentChangeStatus; //打印中途换料状态
  uint8_t ChangeFilamentHeatStatus; //是否加热完成
  uint8_t IsTransFile;  //是否在上传文件
  uint8_t IsSuccessFilament;  //是否完成进丝、退丝
  uint8_t IsFinishedFilamentHeat;  //是否完成加热-进丝、退丝
  uint8_t IsComputerControlToStopPrint; //电脑端控制停止打印
  uint8_t IsComputerControlToResumePrint; //电脑端控制继续打印
  uint8_t IsComputerControlToPausePrint; //电脑端控制暂停打印
  uint8_t IsHomeCalculateZPosLimit;  //是否完成了Z轴的行程测量
  uint8_t IsFinishedCalculateZPosLimit;  //是否完成了Z轴的行程测量
  uint8_t IsFinishedPowerOffRecoverReady; //是否完成了断电续打的准备
  uint8_t IsWarning;  //错误警告
  uint8_t WarningInfoSelect; //警告信息选择
  uint8_t IsRootDir;  //当前目录是否是根目录
  uint8_t IsHaveNextPage;  //是否有下一页
  uint8_t CurrentPage;  //当前GUI要显示的页面-当前目录下的文件分成多个GUI页面
  uint8_t cavity_temp_max_value; //腔体最大温度
  uint8_t IsHaveFile[OnePageNum];//是否有需要显示的文件名
  uint8_t IsDir[OnePageNum];  //当前GUI显示的文件是否是目录文件
  uint8_t G28_ENDSTOPS_COMPLETE; //是否归零完成
  bool m109_heating_complete;             /*!< M109是否加热完成 */
  uint8_t m190_heating_complete; //热床是否加热完成
  uint8_t m305_is_force_verify; //是否强制检验gcode
  uint8_t isOpenBeep; //是否打开蜂鸣器
  uint8_t isLightingOn;
  #ifdef CAL_Z_ZERO_OFFSET
  bool isCalZero;
  #endif
  uint32_t poweroff_data_size;                   // 斷電續打數據緩存大小
  unsigned char is_serial_full;
} T_GUI_P;

typedef struct
{
  int GUISempValue;  //信号量值
  int TargetNozzleTemp; //喷嘴目标温度
  int TargetNozzleTemp1; //喷嘴目标温度
  int TargetHotbedTemp;  //热床目标温度
  int PrintSpeed;  //打印速度
  int FanSpeed;  //风扇速度
  int TargetCavityTemp; //喷嘴目标温度
  int TargetCavityOnTemp; //喷嘴目标温度
  volatile int x_move_value;
  volatile uint8_t active_extruder;
  TCHAR PrintFileName[100];  //选中的打印文件名
  TCHAR DirName[100];  //选中的目录名
} SettingInfo;

typedef struct
{
  char PictureFileBuf[PIC_BUF_SIZE];                                   /*!< 处于IRAM2区的起始地址 40k 大小12k */
  volatile T_SYS_DATA t_sys_data_current;                              /*!<  */
  volatile T_SYS t_sys;                                                /*!<  */
  volatile T_CUSTOM_SERVICES t_custom_services;                        /*!<  */
  volatile motion_3d_t motion_3d;                                      /*!<  */
  volatile motion_3d_model_t motion_3d_model;                          /*!<  */
  volatile block_t block_buffer[BLOCK_BUFFER_SIZE];                    /*!< A ring buffer for motion instfructions */
  volatile unsigned char block_buffer_head;                            /*!< Index of the next block to be pushed */
  volatile unsigned char block_buffer_tail;                            /*!< Index of the block to process now */
  TCHAR power_path_file_name[100];                     // 断电续打文件路徑全名
  TCHAR power_file_name[100];                          // 断电续打文件名
  uint8_t power_is_file_from_sd;                      // 断电续打文件是否在SD卡中
  uint8_t power_is_power_off;                         // 是否已经断电
  uint8_t power_is_data_change;                         // 是否已经断电
  volatile planner_running_status_t runningStatus[BLOCK_BUFFER_SIZE];  /*!< 运动参数数组 */
  volatile planner_running_status_t running_status;                    /*!< 当前运动参数 */
  volatile planner_running_status_t flash_poweroff_recovery;
  volatile planner_running_status_t t_power_off;
  volatile T_GUI_P t_gui_p;
  volatile T_GUI t_gui;                                                /*!<  */
  volatile SettingInfo SettingInfoToSYS;
  volatile char command_buffer[MAX_CMD_SIZE];                          /*!< 指令数组 */
  char os_gcode_buf[MAX_CMD_SIZE];
  #if defined(STM32F429xx)
  char poweroff_data[POWER_OFF_BUF_SIZE];                              /*!< 斷電續打數據緩存  处于IRAM2区的起始地址7k */
  char cmdbuffer[BUFSIZE][MAX_CMD_SIZE];                               /*!< 环形指令队列 */
  unsigned short int TextRangeBuf_24_12_9_0[TEXT_BUF_24_12_9_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_9_1[TEXT_BUF_24_12_9_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_9_2[TEXT_BUF_24_12_9_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_9_3[TEXT_BUF_24_12_9_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_0[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_1[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_2[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_3[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_4[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_5[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_6[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_7[TEXT_BUF_24_12_3_SIZE];    /*!< */
  unsigned short int TextRangeBuf_24_12_3_8[TEXT_BUF_24_12_3_SIZE];    /*!< */
  volatile float grbl_current_position[MAX_NUM_AXIS];                  /*!< */
  volatile float grbl_destination[MAX_NUM_AXIS];                       /*!< */
  volatile int32_t layer_count;                                        /*!< */
  volatile int32_t current_layer;                                      /*!< */
  #elif defined(STM32F407xx)
  volatile uint8_t trans_file_bufs[TRANS_FILE_BUF_SIZE];
  unsigned short NozzleTempTextRangeBuf[24 * 12 * 3];                  /*!< 喷嘴温度显示区域数组，24高12宽3个英文字符 */
  unsigned short HotBedTempTextRangeBuf[24 * 12 * 3];                  /*!< 热床温度显示区域数组，24高12宽3个英文字符 */
  unsigned short NozzleTargetTempTextRangeBuf[24 * 12 * 3];            /*!< 目标喷嘴温度显示区域数组，24高12宽3个英文字符 */
  unsigned short HotBedTargetTempTextRangeBuf[24 * 12 * 3];            /*!< 目标热床温度显示区域数组，24高12宽3个英文字符 */
  unsigned short PrintScheduleShape[24 * 12 * 3];                      /*!< 打印进度显示区域数组，24高12宽3个英文字符 */
  unsigned short CavityTempTextRangeBuf[24 * 12 * 3];                  /*!< 腔体温度显示区域数组，24高12宽3个英文字符 */
  unsigned short CavityTargetTempTextRangeBuf[24 * 12 * 3];            /*!< 目标腔体温度显示区域数组，24高12宽3个英文字符 */
  unsigned short TimeTextRangeBuf[24 * 12 * 11] ;                      /*!< 时间显示区域数组，24高12宽11个英文字符 */
  unsigned short TextRangeBuf[24 * 12 * 11];                           /*!< 数值显示合成区域数组，24高12宽11个英文字符 */
  unsigned short SpeedRangeBuf[24 * 12 * 9];                           /*!< 打印速度显示区域数组，24高12宽9个英文字符 */
  unsigned short XPosRangeBuf[24 * 12 * 3];                            /*!< X位置显示区域数组，24高12宽3个英文字符 */
  unsigned short YPosRangeBuf[24 * 12 * 3];                            /*!< Y位置显示区域数组，24高12宽3个英文字符 */
  unsigned short ZPosRangeBuf[24 * 12 * 3];                            /*!< Z位置显示区域数组，24高12宽3个英文字符 */
  unsigned short PrintSpeedRangeBuf[24 * 12 * 3];                      /*!< 打印速度显示区域数组，24高12宽3个英文字符 */
  unsigned short FanSpeedRangeBuf[24 * 12 * 3];                        /*!< 风扇速度显示区域数组，24高12宽3个英文字符 */
  unsigned short LedSwitchRangeBuf[24 * 12 * 3];                       /*!< led灯显示区域数组，24高12宽3个英文字符 */
  unsigned short CavityTempOnRangeBuf[24 * 12 * 3];                    /*!< 腔体开关显示区域数组，24高12宽3个英文字符 */
  char poweroff_data[POWER_OFF_BUF_SIZE];                              /*!< 斷電續打數據緩存 */
  volatile uint32_t command_buffer_head;                               /*!< 环形指令队列头 */
  volatile uint32_t command_buffer_tail;                               /*!< 环形指令队列尾 */
  char sys_data[512];                                                  /*!< sysconfig數據緩存 */
  #endif
  char file_read_buf[FILE_READ_SIZE];                                  /*!< 读取文件数组对象 */
  volatile int file_read_buf_index;                                    /*!<  */
} ccm_param_t;

extern ccm_param_t ccm_param;
extern void user_ccm_param_init(void);
extern void user_ccm_param_copy_planner_run(planner_running_status_t *A, planner_running_status_t *B);
#ifdef __cplusplus
} //extern "C"
#endif

#endif //USER_CCM_H




