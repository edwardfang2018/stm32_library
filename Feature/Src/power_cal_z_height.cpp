#include "../Inc/power_cal_z_height.h"
#include "user_common.h"
#include "machinecustom.h"
#include "controlxyz.h"
#include "planner.h"
#include "stepper.h"
#include "gcode.h"
#if defined(STM32F429xx)
  #include "process_command.h"
#endif
#ifdef __cplusplus
extern "C" {
#endif

static bool IsStartCalculateZMaxPos = false;                      /*!< 是否开启校正Z高度 */
static bool isStopGetZMax = false;                                /*!< 是否停止校准z高度标志位 */

void feature_pow_cal_z_start(void)
{
  isStopGetZMax = false;
  IsStartCalculateZMaxPos = true; // 设置开始校准Z高度
  #if defined(STM32F429xx)
  ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE = false; // 设置为未归零
  user_send_internal_cmd((char *)"G28 O1"); // XYZ归零
  z_down_to_bottom(); // Z下降到底部
  IsStartCalculateZMaxPos = true; // 设置开始校准Z高度
  #elif defined(STM32F407xx)

  if (!ccm_param.t_sys_data_current.IsMechanismLevel)
  {
    user_send_internal_cmd("G28 X0 Y0 Z0 O1 isInternal");// XYZ归零
    z_down_to_bottom(); // Z下降到底部
    ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE = 0U; // 设置为未归零
  }

  #endif
}

void feature_pow_cal_z_stop(void)
{
  isStopGetZMax = true;
}

void feature_pow_cal_z_process(void)
{
  if (!ccm_param.t_sys_data_current.enable_powerOff_recovery)
    return;

  if (isStopGetZMax)
  {
    stepper_quick_stop(); // 电机快速停止
    IsStartCalculateZMaxPos = false;
    isStopGetZMax = false;
    osDelay(50);
  }

  if (IsStartCalculateZMaxPos) // 开始校准Z
  {
    if (0 == sg_grbl::planner_moves_planned() && sg_grbl::st_check_endstop_z_hit_max() && ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE) //已碰到限位开关  //先归零，然后向下碰限位开关
    {
      const float ZMaxPosValue = sg_grbl::st_get_endstops_len(static_cast<int>(Z_AXIS));
      SaveCalculateZMaxPos(ZMaxPosValue);
      #ifdef ENABLE_GUI_LVGL
      ccm_param.t_gui_p.IsHomeCalculateZPosLimit = 1U;
      #else
      ccm_param.t_gui_p.IsFinishedCalculateZPosLimit = 1U;
      #endif
      IsStartCalculateZMaxPos = false;
      user_send_internal_cmd((char *)"G28");
      //串口上传信息到上位机2017.7.6
      USER_EchoLogStr("z_max_pos:%.2f\r\n", ZMaxPosValue);
      osDelay(50);
    }

    #if defined(STM32F407xx)
    else if (ccm_param.t_sys_data_current.IsMechanismLevel)
    {
      ccm_param.t_gui_p.IsFinishedCalculateZPosLimit = 1U;
      IsStartCalculateZMaxPos = false;
    }

    //#endif
    #endif
  }
}

#ifdef __cplusplus
} //extern "C" {
#endif



















