#include "user_common_cpp.h"
#ifdef HAS_PRINT_CONTROL
#if defined(STM32F429xx)
  #include "process_command.h"
#elif defined(STM32F407xx)
  #include "temperature.h"
  #include "gcode.h"
  #include "Alter.h"
#endif
#include "USBFileTransfer.h"
#include "planner.h"
#include "controlxyz.h"
#include "gcodebufferhandle.h"
#ifdef __cplusplus
extern "C" {
#endif

static bool is_process_stop_print = false;       /*!< 是否执行停止打印 */

bool IsProcessStopPrint(void)
{
  return is_process_stop_print;
}

namespace feature_print_control
{



  void stop_start(void)
  {
    print_status.is_pause_printing = false;
    print_status.is_stop_printing = true;
    file_close();
    #if defined(STM32F429xx)
    #elif defined(STM32F407xx)
    user_fan_control_nozzle_heat_block(false); // 关闭混色5v风扇
    #endif
    SetIsUSBPrintStop(true);
  }

  void stop_prepare(void)
  {
    if (print_status.is_stop_printing) // 串口停止打印或正常停止打印
    {
      #if defined(STM32F407xx)

      if (0U == ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE)
      {
        Zaxis_RunOnce = false;
      }

      #endif
      stepper_quick_stop(); // 电机快速停止

      if (0 == sg_grbl::planner_moves_planned())                    // 运动队列为空，执行以下操作
      {
        print_status.is_stop_printing = false;                   // 设置停止打印状态为false
        print_status.is_printing = false;
        is_process_stop_print = true;             // 执行停止打印标志置为true
        #if defined(STM32F429xx)

        if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
        {
          ccm_param.t_sys.idex_print_type = IDEX_PRINT_TYPE_NORMAL;
        }

        #endif
        return;
      }
    }
  }

  void stop_process(void)
  {
    #if defined(STM32F407xx)
    static bool serial_respond = false;
    #endif
    stop_prepare();                                                                   // 停止打印准备操作
    osDelay(100); // 延时让其他任务执行

    if (is_process_stop_print && 0 == sg_grbl::planner_moves_planned())
    {
      #if defined(STM32F429xx)

      if (flash_param_t.extruder_type == EXTRUDER_TYPE_LASER || flash_param_t.extruder_type == EXTRUDER_TYPE_DRUG)
      {
        user_send_internal_cmd((char *)"M107");
      }

      if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
      {
        user_send_internal_cmd((char *)"T0 S-1");
      }

      z_down_60mm_and_xy_to_zero();

      if (flash_param_t.extruder_type == EXTRUDER_TYPE_LASER)
      {
        user_send_internal_cmd((char *)"T1 S-1");
      }

      #elif defined(STM32F407xx)

      if ((1U == ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE) || !sg_grbl::temperature_get_extruder_current(0))                                                   // 是否已经归零
      {
        z_down_60mm_and_xy_to_zero();                                                        // 平台下降60mm
      }
      else
      {
        user_send_internal_cmd("G28 isInternal"); // XYZ归零
        user_send_internal_cmd("G1 F600 Z60 I0 H0 isInternal");  // Z下降60mm
      }

      #endif
      osDelay(100); // 延时让其他任务执行
      USER_SEND_INTERNAL_CMD_BUF("M84 X Y E B");         // 解锁XYEB
      USER_SEND_INTERNAL_CMD_BUF("M104 T0 S0");
      USER_SEND_INTERNAL_CMD_BUF("M104 T1 S0");
      USER_SEND_INTERNAL_CMD_BUF("M140 S0");
      #if defined(STM32F429xx)
      resetCmdBuf();                                                                 // 重置指令数组
      #elif defined(STM32F407xx)
      gcode::resetCmdBuf();                                                                 // 重置指令数组
      #endif
      ccm_param.t_gui.cura_speed = 0;                                                          // 读取gcode文件获取数值，停止打印则清0
      is_process_stop_print = false;                                                    // 执行停止打印标志置为false
      osDelay(100); // 延时让其他任务执行
      #if defined(STM32F429xx)
      user_send_internal_cmd((char *)"M2004 S1");
      #elif defined(STM32F407xx)
      serial_respond = true;
      user_send_internal_cmd((char *)"M2005 S1");
      #endif
    }

    #if defined(STM32F407xx)

    if (serial_respond && (0 == sg_grbl::planner_moves_planned()))
    {
      serial_respond = false;
    }

    #endif
  }


}
#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_PRINT_CONTROL





