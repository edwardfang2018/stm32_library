#include "user_common_cpp.h"
#ifdef HAS_PRINT_CONTROL
#if defined(STM32F429xx)
  #include "jshdisplay.h"
  #include "process_command.h"
  #ifdef ENABLE_GUI_LVGL
    #include "custom.h"
  #endif
#elif defined(STM32F407xx)
  #include "file_common.h"
  #include "sys_function.h"
  #include "view_common.h"
  #include "file_sgcode.h"
  #include "gcode_global_params.h"
#endif
#include "gcodebufferhandle.h"
#include "controlxyz.h"
#include "USBFileTransfer.h"
#include "stepper.h"
#include "RespondGUI.h"
#ifdef __cplusplus
extern "C" {
#endif

#define SD_CARD 1                                  /*!< SD卡存储介质 */
#define UDISK 2                                    /*!< U盘存储介质 */
static uint8_t medium_id = UDISK;                  /*!< 记录打印文件存储介质 */

#if defined(STM32F429xx)
TCHAR printfilepathname[_MAX_LFN];             /*!< 文件名称 */
FIL printfile;                                /*!< 文件对象 */

#elif defined(STM32F407xx)
extern unsigned long starttime;              /*!< 打印时间计数 */
char printfilepathname[MK_MAX_LFN];             /*!< 文件名称 */
static volatile unsigned long file_pos = 0;          /*!< 文件位置 */
static unsigned char read_data_error_count = 0;      /*!< 读U盘文件异常计数 */


#define FILE_GCODE 0
#define FILE_SGCODE 1
#define FILE_GSD 2
static unsigned char fileType = FILE_GCODE;      /*!< 打印文件类型 */
#define BMP_PATH "0:/file2.bmp"       //提取sgcode中的bmp文件后的存放路径20170920
#endif
namespace feature_print_control
{

  void readBMPFile(const TCHAR *fileName)
  {
    uint32_t RCount;
    MaseHeader header;
    FilesMsg msg;
    FIL *file = new (FIL);
    FIL *file1 = new (FIL);
    FRESULT f_res;
    f_res = f_open(file, fileName, FA_READ);

    if (f_res != FR_OK)
    {
      USER_EchoLogStr("File error!!\t f_res = %d\r\n", f_res);
    }
    else
    {
      // 读取header
      f_read(file, &header, sizeof(MaseHeader), &RCount);
#if defined(STM32F429xx)
      //定位gcode文件
      f_lseek(file, sizeof(MaseHeader) + 1 * sizeof(FilesMsg));
      f_read(file, &msg, sizeof(FilesMsg), &RCount);
      utils_str_memmove((TCHAR *)ccm_param.SettingInfoToSYS.PrintFileName, printname);
      ccm_param.t_gui.printfile_size = msg.uFileSize;

      if (print_status.is_poweroff_recovery_print && (ccm_param.t_power_off.sd_position != 0))
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size - ccm_param.t_power_off.sd_position;
        (void)f_lseek(&printfile, ccm_param.t_power_off.sd_position + msg.uFileOfs);
      }
      else
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size;
        (void)f_lseek(&printfile, msg.uFileOfs);
      }

#elif defined(STM32F407xx)
      int gcodeIndex = 0;

      //定位gcode文件
      for (int i = 0; i < header.uFileCount; ++i)
      {
        f_lseek(file, sizeof(MaseHeader) + (i * sizeof(FilesMsg)));
        f_read(file, &msg, sizeof(FilesMsg), &RCount);

        if (strstr(msg.szFileName, ".gcode"))
        {
          gcodeIndex = i;
          break;
        }
      }

      f_lseek(file, sizeof(MaseHeader) + (gcodeIndex * sizeof(FilesMsg)));
      f_read(file, &msg, sizeof(FilesMsg), &RCount);
      utils_str_memmove((TCHAR *)ccm_param.SettingInfoToSYS.PrintFileName, (TCHAR *)printname);
      ccm_param.t_gui.printfile_size = msg.uFileSize;

      if (print_status.is_poweroff_recovery_print && (ccm_param.t_power_off.sd_position != 0))
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size - ccm_param.t_power_off.sd_position;
        //      (void)f_lseek(&printfile, ccm_param.t_power_off.sd_pos + msg.uFileOfs);
        file_pos = ccm_param.t_power_off.sd_position + msg.uFileOfs;
      }
      else
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size;
        //      (void)f_lseek(&printfile, msg.uFileOfs);
        file_pos = msg.uFileOfs;
      }

#endif
      /*
      USER_EchoLogStr("PrintFileName=   %s\n",(char *)ccm_param.SettingInfoToSYS.PrintFileName);
      USER_EchoLogStr("msg.szFileName=  %s\n",msg.szFileName);
      USER_EchoLogStr("msg.uFileOfs=    %d\n",msg.uFileOfs);
      USER_EchoLogStr("msg.uFileSize=   %d\n",msg.uFileSize);
      USER_EchoLogStr("print_file_size = %d\n",ccm_param.t_gui.printfile_size);
      USER_EchoLogStr("file_size=       %d\n",ccm_param.t_gui.file_size);
      */
      f_close(file);
    }

    delete file;
    delete file1;
    file = NULL;
    file1 = NULL;
  }


  void getFileName(void)
  {
    if (print_status.is_poweroff_recovery_print) // 断电续打恢复打印，设置打印文件名
    {
      if (ccm_param.power_is_file_from_sd)   //断电续打文件在SD卡中
      {
        medium_id = SD_CARD;
        //      IsPowerOffRecoverFileInSD = 1; //打印的是SD卡中的文件
      }

      (void)utils_str_memmove(printfilepathname, ccm_param.power_path_file_name);
    }
    else // 正常打印获取文件名
    {
      if (UDISK == medium_id) // U盘文件
      {
        if (ccm_param.t_gui_p.IsRootDir)
          utils_str_memmove(printfilepathname, (TCHAR *)ccm_param.t_gui_p.CurrentPath);
        else
        {
          utils_str_memmove(printfilepathname, (TCHAR *)ccm_param.t_gui_p.CurrentPath);
          utils_str_strcat(printfilepathname, (TCHAR *)_T("/"));
        }

        utils_str_strcat(printfilepathname, (TCHAR *)ccm_param.SettingInfoToSYS.PrintFileName);
      }
      else if (SD_CARD == medium_id) // SD卡文件
      {
#if defined(STM32F429xx)
        utils_str_memmove(printfilepathname, USERPath);
        utils_str_strcat(printfilepathname, (TCHAR *)ccm_param.t_gui_p.SDFileName);
#elif defined(STM32F407xx)
        (void)snprintf(printfilepathname, sizeof(printfilepathname), "%s", SDPath);
        (void)strcat(printfilepathname, (char *)ccm_param.t_gui_p.SDFileName);
#endif
      }
    }
  }

  bool file_open(void)
  {
    bool file_open_status = true;
    getFileName();  // 获取要打印文件名

    if (SD_CARD == medium_id)
    {
      taskENTER_CRITICAL();
    }

#if defined(STM32F429xx)

    if (f_open(&printfile, printfilepathname, FA_READ) == FR_OK)
    {
      ccm_param.t_gui.printfile_size = f_size(&printfile);

      // 断电续打重设文件打印位置
      if (print_status.is_poweroff_recovery_print && (ccm_param.t_power_off.sd_position != 0))
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size - ccm_param.t_power_off.sd_position;
        (void)f_lseek(&printfile, ccm_param.t_power_off.sd_position);
      }
      else
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size;
        ccm_param.t_sys.print_time_save = 0; // 重置打印时间
      }

      //提取BMP图片
      if (utils_str_strstr((TCHAR *)printfilepathname, (TCHAR *)_T(".sgcode")))
        readBMPFile(printfilepathname);

      resetCmdBuf();
      // 重置gui打印状态
      ccm_param.t_gui.printed_time_sec = 0;
      ccm_param.t_gui.print_percent = 0;
      ccm_param.t_gui.printfile_size = ccm_param.t_gui.printfile_size;
      ccm_param.t_gui.file_size = ccm_param.t_gui.file_size;
      ccm_param.t_gui.used_total_material = 0;
    }

#elif defined(STM32F407xx)

    if (f_open(&SDFile, printfilepathname, FA_READ) == FR_OK)
    {
      ccm_param.t_gui.printfile_size = f_size(&SDFile);

      // 断电续打重设文件打印位置
      if (print_status.is_poweroff_recovery_print && (ccm_param.t_power_off.sd_position != 0))
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size - ccm_param.t_power_off.sd_position;
        (void)f_lseek(&SDFile, ccm_param.t_power_off.sd_position);
        file_pos = ccm_param.t_power_off.sd_position; // 打开文件重置文件位置
      }
      else
      {
        ccm_param.t_gui.file_size = ccm_param.t_gui.printfile_size;
        file_pos = 0; // 打开文件重置文件位置
      }

      //提取BMP图片
      if (strstr(printfilepathname, ".sgcode"))
      {
        readBMPFile(printfilepathname);
      }

      gcode::resetCmdBuf();

      if (strstr(printfilepathname, ".gsd"))
      {
        fileType = FILE_GSD;
      }

      // 重置gui打印状态
      ccm_param.t_gui.printed_time_sec = 0;
      ccm_param.t_gui.print_percent = 0;
      starttime = sys_task_get_tick_count();
      //      (void)f_close(&SDFile);
    }

#endif
    else
    {
      file_open_status = false;
    }

    if (SD_CARD == medium_id)
    {
      taskEXIT_CRITICAL();
    }

    return file_open_status;
  }


  void file_close(void)
  {
#if defined(STM32F429xx)
    ccm_param.t_gui.printfile_size = 0;
    ccm_param.t_gui.file_size = 0;
    ccm_param.file_read_buf_index = FILE_READ_SIZE;
#endif

    if (SD_CARD == medium_id)
    {
      print_status.is_print_sd_file = false;
      ccm_param.power_is_file_from_sd = 0;
      medium_id = UDISK;
      taskENTER_CRITICAL();
#if defined(STM32F429xx)
      (void)f_close(&printfile);
#else
      (void)f_close(&SDFile);
#endif
      (void)f_unlink(printfilepathname); //打印完删除上传的文件
      taskEXIT_CRITICAL();
    }
    else //从U盘读取的时候，不能添加，因为USB的控制是一个单独的任务
    {
#if defined(STM32F429xx)
      (void)f_close(&printfile);
#else
      (void)f_close(&SDFile);
#endif
    }

    //删除BMP图片
    if (utils_str_strstr((TCHAR *)printfilepathname, (TCHAR *)_T(".sgcode")))
    {
      taskENTER_CRITICAL();
      (void)f_unlink(BMP_PATH);
      taskEXIT_CRITICAL();
    }
  }

  bool readDataToBuf(void)
  {
    unsigned int file_br;    /* File R/W count */
    bool result = false;

    if (ccm_param.t_gui.file_size)
    {
      (void)memset(ccm_param.file_read_buf, 0, sizeof(ccm_param.file_read_buf)); //clear buffer

      if (SD_CARD == medium_id)
      {
        taskENTER_CRITICAL();
      }

#if defined(STM32F429xx)

      if (f_read(&printfile, ccm_param.file_read_buf, (ccm_param.t_gui.file_size <= FILE_READ_SIZE) ? ccm_param.t_gui.file_size : sizeof(ccm_param.file_read_buf), &file_br) != FR_OK)
      {
        PopWarningInfo(FatfsWarning);
        result = true;
      }

#elif defined(STM32F407xx)
      //      if (f_open(&SDFile, printfilepathname, FA_READ) == FR_OK)
      //      {
      f_lseek(&SDFile, file_pos);

      if (f_read(&SDFile, ccm_param.file_read_buf, (ccm_param.t_gui.file_size <= FILE_READ_SIZE) ? ccm_param.t_gui.file_size : sizeof(ccm_param.file_read_buf), &file_br) == FR_OK)
      {
        file_pos += (ccm_param.t_gui.file_size <= FILE_READ_SIZE) ? ccm_param.t_gui.file_size : sizeof(ccm_param.file_read_buf);
        read_data_error_count = 0;
      }

      //        f_close(&SDFile);
      //      }
      //      else
      //      {
      //        ++read_data_error_count;
      //        if (read_data_error_count == 3)
      //        {
      //          PopWarningInfo(FatfsWarning);
      //          result = true;
      //        }
      //      }
#endif
      ccm_param.file_read_buf_index = 0;

      if (SD_CARD == medium_id)
      {
        taskEXIT_CRITICAL();
      }
    }

    return result;
  }

  void readFinish(void)
  {
    feature_print_control::file_close();                  // 读取结束，关闭文件
    sg_grbl::st_synchronize();         // 执行完环形队列指令再执行下面指令
    set_file_print_finish();
#if defined(STM32F429xx)
    ccm_param.t_gui.used_total_material += GetCurrentPosition((int)E_AXIS);

    if (1 == ccm_param.t_sys_data_current.enable_color_mixing)
    {
      ccm_param.t_gui.used_total_material += GetCurrentPosition((int)B_AXIS);
    }

    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
    {
      ccm_param.t_sys.idex_print_type = IDEX_PRINT_TYPE_NORMAL;
    }
    else if (flash_param_t.extruder_type == EXTRUDER_TYPE_MIX)
    {
      ccm_param.t_sys.mix_print_type = MIX_PRINT_TYPE_GRADIENT_COLOR;
    }

#elif defined(STM32F407xx)
    ccm_param.t_gui.used_total_material = (int)(gcode::printing_material_length + gcode::get_current_position((int)E_AXIS));
    user_fan_control_nozzle_heat_block(false); // 关闭混色5v风扇
#endif

    if (1 == ccm_param.t_sys_data_current.enable_color_mixing)
    {
      user_send_internal_cmd((char *)"G92 E0 B0");
    }
    else
    {
      user_send_internal_cmd((char *)"G92 E0");
    }

    z_down_60mm_and_xy_to_zero();            // 打印完后下降60
    SetIsUSBPrintFinished(true);     // USB联机已完成打印
  }

  //从SD卡读取gcode命令
  void file_get_gcode_buf(void)
  {
    if (print_status.is_printing)
    {
      while (ccm_param.t_gui.file_size)
      {
        if (ccm_param.file_read_buf_index == FILE_READ_SIZE)
        {
          taskENTER_CRITICAL();
          HAL_NVIC_SetPriority(TIM4_IRQn, 5, 0);
          taskEXIT_CRITICAL();
          osDelay(100);
          bool isReadSuccess = readDataToBuf();
          taskENTER_CRITICAL();
          HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
          taskEXIT_CRITICAL();

          if (isReadSuccess)
          {
            break;    //读取文件失败
          }
        }
        else
        {
#if defined(STM32F429xx)
          char sd_char = ccm_param.file_read_buf[ccm_param.file_read_buf_index++];
          uint32_t file_position =  ccm_param.t_gui.printfile_size - ccm_param.t_gui.file_size;
          ccm_param.t_gui.file_size--;
          ccm_param.t_gui.file_size = ccm_param.t_gui.file_size;

          if (0 == ccm_param.t_gui.file_size)
          {
            readFinish();
          }

          if (GetGcodeFromBuf(sd_char, file_position, (1 == ccm_param.t_sys_data_current.enable_color_mixing && 1 == ccm_param.t_sys_data_current.have_set_machine), ccm_param.t_gui.file_size, (unsigned int &)ccm_param.file_read_buf_index)) // 若是混色版本 且 已经设置好机器，需要解密
            break;

#elif defined(STM32F407xx)

          if (FILE_GSD == fileType)
          {
            if (ccm_param.file_read_buf[ccm_param.file_read_buf_index] > 100)
            {
              ccm_param.file_read_buf[ccm_param.file_read_buf_index] -= 100;
            }
            else
            {
              ccm_param.file_read_buf[ccm_param.file_read_buf_index] = '\n';
            }

            if (ccm_param.file_read_buf[ccm_param.file_read_buf_index] == '?')
            {
              ccm_param.file_read_buf[ccm_param.file_read_buf_index] = '\n';
            }
          }

          ccm_param.t_gui.file_position =  ccm_param.t_gui.printfile_size - ccm_param.t_gui.file_size;
          ccm_param.t_gui.file_size--;

          if (0 == ccm_param.t_gui.file_size)
          {
            readFinish();
            break;
          }

          if (gcode::GetGcodeFromBuf()) // 若是混色版本 且 已经设置好机器，需要解密
          {
            break;
          }

#endif
        }
      }
    }
  }


  /**
  * [PrintFileControl::getPercent 获取文件打印百分比]
  * @return  [description]
  */
  int file_get_percent(void)
  {
    if (ccm_param.t_gui.printfile_size)
    {
      return (int)((ccm_param.t_gui.printfile_size - ccm_param.t_gui.file_size) / ((ccm_param.t_gui.printfile_size + 99) / 100));
    }
    else
    {
      return 0;
    }
  }

  /**
   * [PrintFileControl::setSDMedium 设置当前打印介质为SD卡，联机打印需要用到]
   */
  void file_set_sd_medium(void)
  {
    medium_id = SD_CARD;     // 设置当前文件所在介质为sd卡
    print_status.is_print_sd_file = true; // 设置打印sd文件标志为true
  }

}


#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_PRINT_CONTROL





