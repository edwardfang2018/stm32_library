#include "user_common.h"

void user_motor_delay_us(volatile unsigned short time)
{
  volatile unsigned short i = 0;

  while (time--)
  {
    i = 10; //自己定义

    while (i--) __NOP();
  }
}

#if defined(STM32F407xx)

// 李工新电路板，增加PB3引脚控制电机工作
// PB3使能，电机可正常工作
void user_motor_start_up_pin_init(void)
{
  // PB3引脚，启动电机
  GPIO_InitTypeDef GPIO_InitStruct;
  __GPIOB_CLK_ENABLE();
  GPIO_InitStruct.Pin = STEP_START_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(STEP_START_PIN_GPIO, &GPIO_InitStruct);
}

// 使能pb3，电机可正常工作;
void user_motor_set_start_up_pin_status(const volatile bool on)
{
  digitalWrite(STEP_START_PIN, (on ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

#ifdef ENDSTOPPULLUP_XMAX

// 最大限位上拉
void user_motor_axis_xyz_write_max(void)
{
#if defined(X_MAX_PIN) && (X_MAX_PIN_ON > -1)
  digitalWrite(X_MAX_PIN, GPIO_PIN_SET);
#endif // #if defined(X_MAX_PIN) && (X_MAX_PIN_ON > -1)
#if defined(Y_MAX_PIN) && (Y_MAX_PIN_ON > -1)
  digitalWrite(Y_MAX_PIN, GPIO_PIN_SET);
#endif // #if defined(Y_MAX_PIN) && (Y_MAX_PIN_ON > -1)
#if defined(Z_MAX_PIN) && (Z_MAX_PIN_ON > -1)
  digitalWrite(Z_MAX_PIN, GPIO_PIN_SET);
#endif // #if defined(Z_MAX_PIN) && (Z_MAX_PIN_ON > -1)
}

#endif // #ifdef ENDSTOPPULLUP_XMAX

#ifdef ENDSTOPPULLUP_ZMIN

// 最小限位上拉
void user_motor_axis_xyz_write_min(void)
{
#if defined(X_MIN_PIN) && (X_MIN_PIN_ON > -1)
  digitalWrite(X_MIN_PIN, GPIO_PIN_SET);
#endif // #if defined(X_MIN_PIN) && (X_MIN_PIN_ON > -1)
#if defined(Y_MIN_PIN) && (Y_MIN_PIN_ON > -1)
  digitalWrite(Y_MIN_PIN, GPIO_PIN_SET);
#endif // #if defined(Y_MIN_PIN) && (Y_MIN_PIN_ON > -1)
#if defined(Z_MIN_PIN) && (Z_MIN_PIN_ON > -1)
  digitalWrite(Z_MIN_PIN, GPIO_PIN_SET);
#endif // #if defined(Z_MIN_PIN) && (Z_MIN_PIN_ON > -1)
}

#endif // #ifdef ENDSTOPPULLUP_ZMIN

#elif defined(STM32F429xx)
const static bool endstop_min_inverting[XYZ_NUM_AXIS] = {X_MIN_ENDSTOP_INVERTING, X2_MIN_ENDSTOP_INVERTING,
                                                         Y_MIN_ENDSTOP_INVERTING, Z_MIN_ENDSTOP_INVERTING,
                                                         Z2_MIN_ENDSTOP_INVERTING
                                                        };
const static bool endstop_max_inverting[XYZ_NUM_AXIS] = {X_MAX_ENDSTOP_INVERTING, X2_MAX_ENDSTOP_INVERTING,
                                                         Y_MAX_ENDSTOP_INVERTING, Z_MAX_ENDSTOP_INVERTING,
                                                         Z2_MAX_ENDSTOP_INVERTING
                                                        };

const static bool axis_enable[MAX_NUM_AXIS] = {X_ENABLE_ON, X2_ENABLE_ON, Y_ENABLE_ON, Z_ENABLE_ON,
                                               Z2_ENABLE_ON, E_ENABLE_ON, B_ENABLE_ON
                                              };
const static bool axis_disable[MAX_NUM_AXIS] = {X_ENABLE_OFF, X2_ENABLE_OFF, Y_ENABLE_OFF, Z_ENABLE_OFF,
                                                Z2_ENABLE_OFF, E_ENABLE_OFF, B_ENABLE_OFF
                                               };


const static bool invert_axis_step_pin[MAX_NUM_AXIS] = {INVERT_X_STEP_PIN, INVERT_X2_STEP_PIN,
                                                        INVERT_Y_STEP_PIN, INVERT_Z_STEP_PIN,
                                                        INVERT_Z2_STEP_PIN, INVERT_E_STEP_PIN,
                                                        INVERT_B_STEP_PIN
                                                       };
#endif

bool user_motor_axis_endstop_read_min(int axis)
{
#if defined(STM32F407xx)
  bool result = false;

  if (X_AXIS == axis)
  {
#if defined(X_MIN_PIN) && (X_MIN_PIN_ON > -1)
    result = (int)digitalRead(X_MIN_PIN) != X_MIN_ENDSTOP_INVERTING;

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (int)digitalRead(X_MIN_PIN) != X_MIN_ENDSTOP_INVERTING;
    }

#endif // #if defined(X_MIN_PIN) && (X_MIN_PIN_ON > -1)
  }
  else if (Y_AXIS == axis)
  {
#if defined(Y_MIN_PIN) && (Y_MIN_PIN_ON > -1)
    result = (int)digitalRead(Y_MIN_PIN) != Y_MIN_ENDSTOP_INVERTING;

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (int)digitalRead(Y_MIN_PIN) != Y_MIN_ENDSTOP_INVERTING;
    }

#endif // #if defined(Y_MIN_PIN) && (Y_MIN_PIN_ON > -1)
  }
  else if (Z_AXIS == axis)
  {
#if defined(Z_MIN_PIN) && (Z_MIN_PIN_ON > -1)
    result = (int)digitalRead(Z_MIN_PIN) != Z_MIN_ENDSTOP_INVERTING;

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (int)digitalRead(Z_MIN_PIN) != Z_MIN_ENDSTOP_INVERTING;
    }

#endif // #if defined(Z_MIN_PIN) && (Z_MIN_PIN_ON > -1)
  }

  return result;
#elif defined(STM32F429xx)

  if (axis < XYZ_NUM_AXIS)
  {
    USER_ENDSTOP_READ(MOTOR_LIMIT_MIN_PIN, axis, endstop_min_inverting[axis]);
  }

  return false;
#endif
}

bool user_motor_axis_endstop_read_max(int axis)
{
#if defined(STM32F407xx)
  bool result = false;

  if (X_AXIS == axis)
  {
#if defined(X_MAX_PIN) && (X_MAX_PIN_ON > -1)
    result = (int)digitalRead(X_MAX_PIN) != X_MAX_ENDSTOP_INVERTING;

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (int)digitalRead(X_MAX_PIN) != X_MAX_ENDSTOP_INVERTING;
    }

#endif // #if defined(X_MAX_PIN) && (X_MAX_PIN_ON > -1)
  }
  else if (Y_AXIS == axis)
  {
#if defined(Y_MAX_PIN) && (Y_MAX_PIN_ON > -1)
    result = (int)digitalRead(Y_MAX_PIN) != Y_MAX_ENDSTOP_INVERTING;

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (int)digitalRead(Y_MAX_PIN) != Y_MAX_ENDSTOP_INVERTING;
    }

#endif // #if defined(Y_MAX_PIN) && (Y_MAX_PIN_ON > -1)
  }
  else if (Z_AXIS == axis)
  {
    //M3145S开启断料、腔体，断料使用ZMAX触发
    if (ccm_param.t_sys_data_current.model_id == M3145S && 1 == ccm_param.t_sys_data_current.enable_cavity_temp && ccm_param.t_sys_data_current.enable_material_check)
    {
      return result;
    }

#if defined(Z_MAX_PIN) && (Z_MAX_PIN_ON > -1)
    result = (int)digitalRead(Z_MAX_PIN) != Z_MAX_ENDSTOP_INVERTING;

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (int)digitalRead(Z_MAX_PIN) != Z_MAX_ENDSTOP_INVERTING;
    }

#endif // #if defined(Z_MAX_PIN) && (Z_MAX_PIN_ON > -1)
  }

  return result;
#elif defined(STM32F429xx)

  if (axis < XYZ_NUM_AXIS)
  {
    USER_ENDSTOP_READ(MOTOR_LIMIT_MAX_PIN, axis, endstop_max_inverting[axis]);
  }

  return false;
#endif
}

void user_motor_axis_enable(int axis, bool isEnable)
{
#if defined(STM32F407xx)

  if (X_AXIS == axis)
  {
#if defined(X_ENABLE_PIN) && (X_ENABLE_PIN_ON > -1)

    if (!DISABLE_X)
    {
      digitalWrite(X_ENABLE_PIN, (isEnable ? X_ENABLE_ON : X_ENABLE_OFF));
    }

#endif // #if defined(X_ENABLE_PIN) && (X_ENABLE_PIN_ON > -1)
  }
  else if (Y_AXIS == axis)
  {
#if defined(Y_ENABLE_PIN) && (Y_ENABLE_PIN_ON > -1)

    if (!DISABLE_Y)
    {
      digitalWrite(Y_ENABLE_PIN, (isEnable ? Y_ENABLE_ON : Y_ENABLE_OFF));
    }

#endif // #if defined(Y_ENABLE_PIN) && (Y_ENABLE_PIN_ON > -1)
  }
  else if (Z_AXIS == axis)
  {
#if defined(Z_ENABLE_PIN) && (Z_ENABLE_PIN_ON > -1)

    if (!DISABLE_Z)
    {
      digitalWrite(Z_ENABLE_PIN, (isEnable ? Z_ENABLE_ON : Z_ENABLE_OFF));
    }

#endif // #if defined(Z_ENABLE_PIN) && (Z_ENABLE_PIN_ON > -1)
  }
  else if (E_AXIS == axis)
  {
#if defined(E0_ENABLE_PIN) && (E0_ENABLE_PIN_ON > -1)

    if (!DISABLE_E)
    {
      digitalWrite(E0_ENABLE_PIN, (isEnable ? E_ENABLE_ON : E_ENABLE_OFF));
    }

#endif // #if defined(E0_ENABLE_PIN) && (E0_ENABLE_PIN_ON > -1)
  }
  else if (B_AXIS == axis)
  {
#if defined(E1_ENABLE_PIN) && (E1_ENABLE_PIN_ON > -1)

    if (!DISABLE_B)
    {
      digitalWrite(E1_ENABLE_PIN, (isEnable ? B_ENABLE_ON : B_ENABLE_OFF));
    }

#endif // #if defined(E1_ENABLE_PIN) && (E1_ENABLE_PIN_ON > -1)
  }

#elif defined(STM32F429xx)
  bool value = (isEnable ? axis_enable[axis] : axis_disable[axis]);
  USER_MOTOR_WRITE(MOTOR_EN_PIN, axis, value);
#endif
}

void user_motor_axis_write_step(int axis, bool value)
{
#if defined(STM32F407xx)

  if (X_AXIS == axis)
  {
#if defined(X_STEP_PIN) && (X_STEP_PIN_ON > -1)
    digitalWrite(X_STEP_PIN, value);
#endif // #if defined(X_STEP_PIN) && (X_STEP_PIN_ON > -1)
  }
  else if (Y_AXIS == axis)
  {
#if defined(Y_STEP_PIN) && (Y_STEP_PIN_ON > -1)
    digitalWrite(Y_STEP_PIN, value);
#endif // #if defined(Y_STEP_PIN) && (Y_STEP_PIN_ON > -1)
  }
  else if (Z_AXIS == axis)
  {
#if defined(Z_STEP_PIN) && (Z_STEP_PIN_ON > -1)
    digitalWrite(Z_STEP_PIN, value);
#endif // #if defined(Z_STEP_PIN) && (Z_STEP_PIN_ON > -1)
  }
  else if (E_AXIS == axis)
  {
#if defined(E0_STEP_PIN) && (E0_STEP_PIN_ON > -1)
    digitalWrite(E0_STEP_PIN, value);
#endif // #if defined(E0_STEP_PIN) && (E0_STEP_PIN_ON > -1)
  }
  else if (B_AXIS == axis)
  {
#if defined(E1_STEP_PIN) && (E1_STEP_PIN_ON > -1)
    digitalWrite(E1_STEP_PIN, value);
#endif // #if defined(E1_STEP_PIN) && (E1_STEP_PIN_ON > -1)
  }

#elif defined(STM32F429xx)
  USER_MOTOR_WRITE(MOTOR_STEP_PIN, axis, value);
#endif
}

void user_motor_axis_write_dir(int axis, bool isInvert)
{
#if defined(STM32F407xx)

  if (X_AXIS == axis)
  {
#if defined(X_DIR_PIN) && (X_DIR_PIN_ON > -1)
    digitalWrite(X_DIR_PIN, isInvert);
#endif // #if defined(X_DIR_PIN) && (X_DIR_PIN_ON > -1)
  }
  else if (Y_AXIS == axis)
  {
#if defined(Y_DIR_PIN) && (Y_DIR_PIN_ON > -1)
    digitalWrite(Y_DIR_PIN, isInvert);
#endif // #if defined(Y_DIR_PIN) && (Y_DIR_PIN_ON > -1)
  }
  else if (Z_AXIS == axis)
  {
#if defined(Z_DIR_PIN) && (Z_DIR_PIN_ON > -1)
    digitalWrite(Z_DIR_PIN, isInvert);
#endif // #if defined(Z_DIR_PIN) && (Z_DIR_PIN_ON > -1)
  }
  else if (E_AXIS == axis)
  {
#if defined(E0_DIR_PIN) && (E0_DIR_PIN_ON > -1)
    digitalWrite(E0_DIR_PIN, isInvert);
#endif // #if defined(E0_DIR_PIN) && (E0_DIR_PIN_ON > -1)
  }
  else if (B_AXIS == axis)
  {
#if defined(E1_DIR_PIN) && (E1_DIR_PIN_ON > -1)
    digitalWrite(E1_DIR_PIN, isInvert);
#endif // #if defined(E1_DIR_PIN) && (E1_DIR_PIN_ON > -1)
  }

#elif defined(STM32F429xx)
  GPIO_PinState value = (GPIO_PinState)(isInvert ? ccm_param.motion_3d_model.enable_invert_dir[axis] :
                                        !ccm_param.motion_3d_model.enable_invert_dir[axis]);
  USER_MOTOR_WRITE(MOTOR_DIR_PIN, axis, value);
#endif
}

void user_motor_pin_init(void)
{
#if defined(STM32F407xx)
  // 启动电机引脚初始化
  user_motor_start_up_pin_init();
  // 延时200ms后锁定z电机
  (void)user_motor_delay_us(200);
  // 使能pb3，电机可正常工作
  user_motor_set_start_up_pin_status(true);
  //endstops and pullups
#ifdef ENDSTOPPULLUP_ZMIN
  user_motor_axis_xyz_write_min();
#endif
#ifdef ENDSTOPPULLUP_XMAX
  user_motor_axis_xyz_write_max();
#endif

  // 使能电机
  for (int i = 0; i < ccm_param.motion_3d.axis_num; ++i)
  {
    user_motor_axis_enable(i, true);
  }

  //Initialize Step Pins
  user_motor_axis_write_step(X_AXIS, INVERT_X_STEP_PIN);
  user_motor_axis_write_step(Y_AXIS, INVERT_Y_STEP_PIN);
  user_motor_axis_write_step(Z_AXIS, INVERT_Z_STEP_PIN);
  user_motor_axis_write_step(E_AXIS, INVERT_E_STEP_PIN);
  user_motor_axis_write_step(B_AXIS, INVERT_B_STEP_PIN);

  // 解锁电机
  for (int i = 0; i < ccm_param.motion_3d.axis_num; ++i)
  {
    user_motor_axis_enable(i, false);
  }

#elif defined(STM32F429xx)

  //endstops and pullups
  for (int i = 0; i < XYZ_NUM_AXIS; i++)
  {
    USER_MOTOR_WRITE(MOTOR_LIMIT_MIN_PIN, i, GPIO_PIN_SET);
  }

  //Initialize Step Pins
  for (int i = 0; i < MAX_NUM_AXIS; i++)
  {
    user_motor_axis_enable(i, true);
    user_motor_axis_write_step(i, invert_axis_step_pin[i]);
    user_motor_axis_enable(i, false);
  }

#endif
}

void user_motor_timer_start(void)
{
  extern TIM_HandleTypeDef htim4;
  HAL_TIM_Base_Start_IT(&htim4);
}

void user_motor_timer_stop(void)
{
  extern TIM_HandleTypeDef htim4;
  HAL_TIM_Base_Stop_IT(&htim4);
}

void user_motor_timer_set_period(uint32_t period_value)
{
  extern TIM_HandleTypeDef htim4;
  htim4.Instance->ARR = period_value;
  htim4.Init.Period = period_value;
}



