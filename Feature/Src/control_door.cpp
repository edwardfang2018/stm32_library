#include "user_common_cpp.h"
#ifdef HAS_DOOR_CONTROL
#include "temperature.h"
#include "planner.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(STM32F429xx)
#include "process_m_code.h"
#elif defined(STM32F407xx)

#endif

//警示灯控制
static void _feature_control_door_caution_light(void)
{
  static uint8_t LastStatus = 0U;
  static bool doorOpenBeep = false;//标志门检打开了蜂鸣器

  if (ccm_param.t_gui_p.doorStatus && print_status.is_printing) //打印中门未关闭
  {
    feature_control_led_caution_twinkle(100U);//0.1S闪烁
    LastStatus = 1U;
    ccm_param.t_gui_p.IsDisplayDoorOpenInfo = 0U;
  }
  else if (print_status.is_printing && ccm_param.t_gui_p.m109_heating_complete) //加热完成并开始打印
  {
    #if defined(STM32F407xx)
    user_led_control_caution_status(true);
    #endif
    LastStatus = 2U;
    ccm_param.t_gui_p.IsDisplayDoorOpenInfo = 0U;
  }
  else if ((int)sg_grbl::temperature_get_extruder_current(0) > 60 || print_status.is_printing) //温度大于60度 或 打印前的加热阶段
  {
    if (ccm_param.t_gui_p.doorStatus)
    {
      #if defined(STM32F429xx)
      ccm_param.t_gui_p.isOpenBeep = 1;
      #elif defined(STM32F407xx)
      ccm_param.t_gui_p.isBeepAlarm = 1;
      user_buzzer_control(false);
      #endif
      doorOpenBeep = true;
      feature_control_led_caution_twinkle(100U);//0.1S闪烁
    }
    else
    {
      if (doorOpenBeep)
      {
        #if defined(STM32F429xx)
        ccm_param.t_gui_p.isOpenBeep = 0;
        #elif defined(STM32F407xx)
        ccm_param.t_gui_p.isBeepAlarm = 0;
        #endif
        doorOpenBeep = false;
      }

      feature_control_led_caution_twinkle(500U);//0.5S闪烁
    }

    if (((int)sg_grbl::temperature_get_extruder_current(0) > 60) && !print_status.is_printing && ccm_param.t_gui_p.doorStatus) //待机、预热、进丝、退丝 时 温度大于60度且门打开显示提示信息
    {
      ccm_param.t_gui_p.IsDisplayDoorOpenInfo = 1U;
    }
    else
    {
      ccm_param.t_gui_p.IsDisplayDoorOpenInfo = 0U;
    }

    LastStatus = 3U;
  }
  else //待机且温度小于60度
  {
    if ((LastStatus == 3) && ccm_param.t_gui_p.doorStatus)
    {
      #if defined(STM32F429xx)
      ccm_param.t_gui_p.isOpenBeep = 0;
      #elif defined(STM32F407xx)
      ccm_param.t_gui_p.isBeepAlarm = 0;
      #endif
    }

    #if defined(STM32F407xx)
    user_led_control_caution_status(false);
    #endif
    LastStatus = 4U;
    ccm_param.t_gui_p.IsDisplayDoorOpenInfo = 0U;
  }
}


void feature_control_door(void)
{
  if ((0U != ccm_param.motion_3d.enable_check_door_open))
  {
    _feature_control_door_caution_light();
  }
}

#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_DOOR_CONTROL














