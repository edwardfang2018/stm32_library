#include "user_common_cpp.h"
#ifdef HAS_LED_CONTROL
#include "temperature.h"
#include "planner.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(STM32F429xx)
#include "process_m_code.h"
#elif defined(STM32F407xx)
#endif

static volatile bool led_lighting_status = false;

//主板灯
static void _feature_control_led_board(void)
{
  static unsigned long led_last_time = 0UL;

  if (led_last_time < xTaskGetTickCount())
  {
    user_led_board_toggle();
    led_last_time = xTaskGetTickCount() + 500U; // 500ms闪烁一次
  }
}

//警示灯闪烁
void feature_control_led_caution_twinkle(uint32_t delay_time)
{
  if (0U != ccm_param.t_custom_services.enable_warning_light)
  {
    static unsigned long CautionLightTimeControl = 0UL;

    if (CautionLightTimeControl < xTaskGetTickCount())
    {
      user_led_caution_toggle();
      CautionLightTimeControl = xTaskGetTickCount() + delay_time;
    }
  }
}

//照明灯条控制
static void _feature_control_led_lighting(void)
{
  static uint8_t LEDLight_status = 0U;
  static unsigned long LEDLight_timeoutToStatus = 0UL;

  if (sg_grbl::planner_moves_planned() || feature_print_control::print_is_heating()) // 移动或加热，灯条开启
  {
    if (LEDLight_status == 0)
    {
      #if defined(STM32F429xx)
      user_led_control_lighting_status(true);
      #elif defined(STM32F407xx)
      led_lighting_status = true;
      user_led_control_lighting_status(led_lighting_status);
      #endif
      ccm_param.t_gui_p.isLightingOn = 1;
      LEDLight_status = 1U;
    }

    LEDLight_timeoutToStatus = xTaskGetTickCount() + (60UL * 1000UL); //1 minute timeout
  }
  else
  {
    if ((LEDLight_status == 1U) && (LEDLight_timeoutToStatus < xTaskGetTickCount()))
    {
      #if defined(STM32F429xx)
      user_led_control_lighting_status(false);
      #elif defined(STM32F407xx)
      led_lighting_status = false;
      user_led_control_lighting_status(led_lighting_status);
      #endif
      ccm_param.t_gui_p.isLightingOn = 0;
      LEDLight_status = 0U;
    }
  }
}

//照明灯条状态设置
void feature_control_led_set_lighting_status(bool value)
{
  led_lighting_status = value;
}

//照明灯条状态获取
bool feature_control_led_get_lighting_status(void)
{
  return led_lighting_status;
}

//LED控制入口
void feature_control_led(void)
{
  _feature_control_led_board(); // 核心板LED一直开启

  if (0U != ccm_param.t_custom_services.enable_led_light) // 有LED灯条功能，开启LED灯条照明
  {
    _feature_control_led_lighting();
  }
}

#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_LED_CONTROL














