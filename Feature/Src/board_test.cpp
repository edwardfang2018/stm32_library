#include "user_common_cpp.h"
#include "gcode_global_params.h"
#include "stepper.h"
#include "planner.h"
#include "view_commonf.h"              //包含界面函数
#include "view_common.h"
#include "temperature_pin.h"

#ifdef HAS_BOARD_TEST
#ifdef __cplusplus
extern "C" {
#endif

static volatile bool board_test_motor_is_process = false;

extern volatile long endstops_trigsteps[MAX_NUM_AXIS];
extern volatile long count_position[MAX_NUM_AXIS];

void board_test_motor_set_value(float value)
{
  if (ccm_param.motion_3d.enable_board_test)
  {
    if (!user_motor_axis_endstop_read_min(X_AXIS))
    {
      gcode::current_position[X_AXIS] = value;
    }

    if (!user_motor_axis_endstop_read_min(Y_AXIS))
    {
      gcode::current_position[Y_AXIS] = value;
    }

    if (!user_motor_axis_endstop_read_min(Z_AXIS))
    {
      gcode::current_position[Z_AXIS] = value;
    }

    if (!HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15))
    {
      gcode::current_position[E_AXIS] = value;
      gcode::current_position[B_AXIS] = value;
    }
  }
}

void board_test_motor_check_endstop(void)
{
  if (ccm_param.motion_3d.enable_board_test)
  {
    if (user_motor_axis_endstop_read_min(X_AXIS))
    {
      endstops_trigsteps[X_AXIS] = count_position[X_AXIS];
    }

    if (user_motor_axis_endstop_read_min(Y_AXIS))
    {
      endstops_trigsteps[Y_AXIS] = count_position[Y_AXIS];
    }

    if (user_motor_axis_endstop_read_min(Z_AXIS))
    {
      endstops_trigsteps[Z_AXIS] = count_position[Z_AXIS];
    }

    if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15))
    {
      endstops_trigsteps[E_AXIS] = count_position[E_AXIS];
      endstops_trigsteps[B_AXIS] = count_position[B_AXIS];
    }
  }
}

void board_test_motor_run(void)
{
  if (ccm_param.motion_3d.enable_board_test)
  {
    board_test_motor_is_process = true;
    sg_grbl::st_enable_endstops(false);
    gcode::current_position[X_AXIS] = 0.0f;
    gcode::current_position[Y_AXIS] = 0.0f;
    gcode::current_position[Z_AXIS] = 0.0f;
    gcode::current_position[E_AXIS] = 0.0f;
    gcode::current_position[B_AXIS] = 0.0f;
    sg_grbl::planner_set_position(gcode::current_position);
    board_test_motor_set_value(50.0f);
    gcode::process_buffer_line_normal(gcode::current_position, 3600 / 60);
    sg_grbl::st_synchronize();
    board_test_motor_set_value(0.0f);
    gcode::process_buffer_line_normal(gcode::current_position, 3600 / 60);
    sg_grbl::st_synchronize();
    board_test_motor_is_process = false;
  }
}

void board_test_fan_run(void)
{
  static unsigned long fan_toggle_timeout = 0;
  static bool is_fan_on = false;

  if (fan_toggle_timeout < xTaskGetTickCount())
  {
    fan_toggle_timeout = xTaskGetTickCount() + 1000;

    if (!is_fan_on)
    {
      user_fan_disable_all();
    }
    else
    {
      user_fan_enable_all();
    }

    is_fan_on = !is_fan_on;
  }
}

void board_test_light_run(void)
{
  static unsigned long light_toggle_timeout = 0;
  static bool is_light_on = false;

  if (light_toggle_timeout < xTaskGetTickCount())
  {
    light_toggle_timeout = xTaskGetTickCount() + 1000;

    if (!is_light_on)
    {
      user_led_control_lighting_status(false);
    }
    else
    {
      user_led_control_lighting_status(true);
    }

    is_light_on = !is_light_on;
  }
}


void board_test_main(void)
{
  if (ccm_param.motion_3d.enable_board_test)
  {
    if (!board_test_motor_is_process)
    {
      user_send_internal_cmd("M2010 isInternal"); // 电机正转反转
    }

    board_test_fan_run();
    board_test_light_run();
    osDelay(50);
  }
}

void gui_board_test(void)
{
  char TextBuffer[20];

  if (gui_is_refresh())
  {
    display_picture(130);
    SetTextDisplayRange(132, 36, 12 * 3, 24, &NozzleTempTextRange); //设置显示区域
    SetTextDisplayRange(324, 36, 12 * 3, 24, &HotBedTempTextRange);
    SetTextDisplayRange((132 + (12 * 4)) + (ccm_param.t_sys.lcd_ssd1963_43_480_272 ? 10 : 0), 36, 12 * 3, 24, &NozzleTargetTempTextRange);
    SetTextDisplayRange((324 + (12 * 4)) + (ccm_param.t_sys.lcd_ssd1963_43_480_272 ? 10 : 0), 36, 12 * 3, 24, &HotBedTargetTempTextRange);
    ReadTextDisplayRangeInfo(NozzleTempTextRange, ccm_param.NozzleTempTextRangeBuf); //从lcd读取像素，保存到数组
    ReadTextDisplayRangeInfo(HotBedTempTextRange, ccm_param.HotBedTempTextRangeBuf);
    ReadTextDisplayRangeInfo(NozzleTargetTempTextRange, ccm_param.NozzleTargetTempTextRangeBuf);
    ReadTextDisplayRangeInfo(HotBedTargetTempTextRange, ccm_param.HotBedTargetTempTextRangeBuf);
  }

  if (touchxy(108, 176, 196, 242)) // 预热
  {
    temperature_set_extruders_heater_status(true, 0);
    temperature_set_bed_heater_status(true);
    user_send_internal_cmd("M104 T0 S230 isInternal");
    user_send_internal_cmd("M140 S120 isInternal");
  }

  if (touchxy(280, 176, 366, 242)) // 冷却
  {
    temperature_set_extruders_heater_status(false, 0);
    temperature_set_bed_heater_status(false);
    user_send_internal_cmd("M104 T0 S0 isInternal");
    user_send_internal_cmd("M140 S0 isInternal");
  }

  if (gui_is_rtc())
  {
    //显示喷嘴温度
    snprintf(TextBuffer, sizeof(TextBuffer), "%3d", (int)ccm_param.t_gui.nozzle_temp[0]);
    CopyTextDisplayRangeInfo(NozzleTempTextRange, ccm_param.NozzleTempTextRangeBuf, ccm_param.TextRangeBuf);
    DisplayTextInRangeDefault((unsigned char *)TextBuffer, NozzleTempTextRange, ccm_param.TextRangeBuf);
    //显示斜杠
    DisplayTextDefault((unsigned char *)"/", (132 + (12 * 3)) + (ccm_param.t_sys.lcd_ssd1963_43_480_272 ? 5 : 0), 36); //直接显示到lcd
    //显示喷嘴目标温度
    snprintf(TextBuffer, sizeof(TextBuffer), "%3d", (int)ccm_param.t_gui.target_nozzle_temp[0]);
    CopyTextDisplayRangeInfo(NozzleTargetTempTextRange, ccm_param.NozzleTargetTempTextRangeBuf, ccm_param.TextRangeBuf);
    DisplayTextInRangeDefault((unsigned char *)TextBuffer, NozzleTargetTempTextRange, ccm_param.TextRangeBuf);

    if (!ccm_param.t_custom_services.disable_hot_bed)
    {
      //显示热床温度
      snprintf(TextBuffer, sizeof(TextBuffer), "%3d", (int)ccm_param.t_gui.hot_bed_temp);
      CopyTextDisplayRangeInfo(HotBedTempTextRange, ccm_param.HotBedTempTextRangeBuf, ccm_param.TextRangeBuf);
      DisplayTextInRangeDefault((unsigned char *)TextBuffer, HotBedTempTextRange, ccm_param.TextRangeBuf);
      //显示斜杠
      DisplayTextDefault((unsigned char *)"/", (324 + (12 * 3)) + (ccm_param.t_sys.lcd_ssd1963_43_480_272 ? 5 : 0), 36);
      //显示热床目标温度
      snprintf(TextBuffer, sizeof(TextBuffer), "%3d", (int)ccm_param.t_gui.target_hot_bed_temp);
      CopyTextDisplayRangeInfo(HotBedTargetTempTextRange, ccm_param.HotBedTargetTempTextRangeBuf, ccm_param.TextRangeBuf);
      DisplayTextInRangeDefault((unsigned char *)TextBuffer, HotBedTargetTempTextRange, ccm_param.TextRangeBuf);
    }
  }
}

void board_test_init(void)
{
  ccm_param.motion_3d.enable_board_test = false;

  if (Test == ccm_param.t_sys_data_current.model_id)
  {
    ccm_param.t_sys_data_current.have_set_machine = 0;
    ccm_param.motion_3d.enable_board_test = true;
    gui_set_curr_display(gui_board_test);
  }
}


#ifdef __cplusplus
} //extern "C" {
#endif
#endif



