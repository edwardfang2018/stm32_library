#include "user_common.h"

#ifdef ENABLE_CCM

//IRAM2共64K，起始地址0x10000000  IRAM1共128K，起始地址0x20000000
ccm_param_t ccm_param  __attribute__((at(0X10000000)));

void user_ccm_param_init(void)
{
  memset(ccm_param.PictureFileBuf, 0, sizeof(char)*PIC_BUF_SIZE);
  memset((T_SYS_DATA *)&ccm_param.t_sys_data_current, 0, sizeof(T_SYS_DATA));
  memset((T_SYS *)&ccm_param.t_sys, 0, sizeof(T_SYS));
  memset((T_CUSTOM_SERVICES *)&ccm_param.t_custom_services, 0, sizeof(T_CUSTOM_SERVICES));
  memset((motion_3d_t *)&ccm_param.motion_3d, 0, sizeof(motion_3d_t));
  memset((motion_3d_model_t *)&ccm_param.motion_3d_model, 0, sizeof(motion_3d_model_t));
  memset((block_t *)&ccm_param.block_buffer[0], 0, sizeof(block_t) * BLOCK_BUFFER_SIZE);
  ccm_param.block_buffer_head = 0U;
  ccm_param.block_buffer_tail = 0U;
  memset((planner_running_status_t *)&ccm_param.t_power_off, 0, sizeof(planner_running_status_t));
  memset((planner_running_status_t *)&ccm_param.flash_poweroff_recovery, 0, sizeof(planner_running_status_t));
  memset((planner_running_status_t *)&ccm_param.runningStatus[0], 0, sizeof(planner_running_status_t) * BLOCK_BUFFER_SIZE);
  memset((planner_running_status_t *)&ccm_param.running_status, 0, sizeof(planner_running_status_t));
  memset((T_GUI_P *)&ccm_param.t_gui_p, 0, sizeof(T_GUI_P));
  ccm_param.t_gui_p.cavity_temp_max_value = 50;
  memset((T_GUI *)&ccm_param.t_gui, 0, sizeof(T_GUI));
  memset((SettingInfo *)&ccm_param.SettingInfoToSYS, 0, sizeof(SettingInfo));
  memset((char *)ccm_param.command_buffer, 0, sizeof(char)*MAX_CMD_SIZE);
  #if defined(STM32F407xx)
  memset(ccm_param.NozzleTempTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.HotBedTempTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.NozzleTargetTempTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.HotBedTargetTempTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.PrintScheduleShape, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.CavityTempTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.CavityTargetTempTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 3);
  memset(ccm_param.TimeTextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 11);
  memset(ccm_param.TextRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 11);
  memset(ccm_param.SpeedRangeBuf, 0, sizeof(unsigned short) * 24 * 12 * 9);
  memset(ccm_param.poweroff_data, 0, sizeof(char)*POWER_OFF_BUF_SIZE);
  ccm_param.command_buffer_head = 1U;
  ccm_param.command_buffer_tail = 0U;
  ccm_param.power_is_file_from_sd = 0;
  memset(ccm_param.sys_data, 0, sizeof(char) * 512);
  #endif
  memset(ccm_param.file_read_buf, 0, sizeof(char)*FILE_READ_SIZE);
  ccm_param.file_read_buf_index = FILE_READ_SIZE;
  ccm_param.power_is_data_change = 0;
  memset(ccm_param.os_gcode_buf, 0, sizeof(ccm_param.os_gcode_buf));
}

void user_ccm_param_copy_planner_run(planner_running_status_t *A, planner_running_status_t *B)
{
	for(int i = 0; i< MAX_NUM_AXIS; i++)
	{
		A->axis_position[i] = B->axis_position[i];
		A->axis_real_position[i] = B->axis_real_position[i];
	}

	A->z_real_change_value = B->z_real_change_value;
	A->target_bed_temp = B->target_bed_temp;
	A->target_extruder0_temp = B->target_extruder0_temp;
	A->target_extruder1_temp = B->target_extruder1_temp;
	A->feed_rate = B->feed_rate;
	A->sd_position = B->sd_position;
	A->print_time_save = B->print_time_save;
	
	A->extruder_multiply = B->extruder_multiply;
	A->feed_multiply = B->feed_multiply;
	A->fan_speed = B->fan_speed;
	A->layer_count = B->layer_count;
	A->current_layer = B->current_layer;
	A->enable_color_buf = B->enable_color_buf;
	
	A->active_extruder = B->active_extruder;
	A->is_serial = B->is_serial;
	A->flag = B->flag;
	

}


#endif



























