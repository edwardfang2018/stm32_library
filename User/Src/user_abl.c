#include "user_common.h"
#ifdef ENABLE_ABL

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim10;

void user_abl_deinit(void)
{
  #if defined(STM32F407xx)
  GPIO_InitTypeDef GPIO_InitStruct;
  /*Configure GPIO pin : PE2*/
  GPIO_InitStruct.Pin = E1_STEP_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(E1_STEP_PIN_GPIO, &GPIO_InitStruct);
  #endif
}

void user_abl_init(void)
{
//  if(ccm_param.t_sys.is_abl_init == 0) return;
  #if defined(STM32F407xx)

  if ((2 == ccm_param.t_sys_data_current.enable_bed_level) && (ccm_param.t_sys_data_current.model_id == K5))
  {
    if (1 == ccm_param.t_sys_data_current.IsMechanismLevel) // 黑屏固件，机械调平
    {
      #ifdef CAL_Z_ZERO_OFFSET
      GPIO_InitTypeDef GPIO_InitStruct;
      HAL_GPIO_DeInit(GPIOA, GPIO_PIN_6);
      /*Configure GPIO pin : PE2*/
      GPIO_InitStruct.Pin = GPIO_PIN_6;
      GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
      GPIO_InitStruct.Pull = GPIO_PULLUP;
      HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
      #endif
    }
    else if (0 == ccm_param.t_sys_data_current.IsMechanismLevel) // 黑屏固件，红外调平
    {
      #ifdef CAL_Z_ZERO_OFFSET
      GPIO_InitTypeDef GPIO_InitStruct;
      // 开启红外检测
      HAL_GPIO_DeInit(E1_STEP_PIN_GPIO, E1_STEP_PIN);
      /*Configure GPIO pin : PE2*/
      GPIO_InitStruct.Pin = E1_STEP_PIN;
      GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
      GPIO_InitStruct.Pull = GPIO_PULLDOWN;
      HAL_GPIO_Init(E1_STEP_PIN_GPIO, &GPIO_InitStruct);
      #endif
    }
  }

  #elif defined(STM32F429xx)

  // Start steering gear control double head printing on the P2_Pro machine
  if (P2_Pro == ccm_param.t_sys_data_current.model_id)
  {
    if (mcu_id == MCU_GD32F450IIH6)
    {
      HAL_TIM_PWM_Start(&htim10, TIM_CHANNEL_1);
    }
    else
    {
      HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
    }
  }
  else if (F300TP == ccm_param.t_sys_data_current.model_id)
  {
    ccm_param.t_sys.enable_cavity_temp = 0; //
    HAL_GPIO_DeInit(GD32_TEMP_CAVITY_GPIO_Port, GD32_TEMP_CAVITY_Pin);
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOB_CLK_ENABLE();
    GPIO_InitStruct.Pin = GD32_TEMP_CAVITY_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GD32_TEMP_CAVITY_GPIO_Port, &GPIO_InitStruct);
  }
  else if (K5 == ccm_param.t_sys_data_current.model_id)
  {
    if (1 == ccm_param.t_sys_data_current.IsMechanismLevel) // 黑屏固件，机械调平
    {
      ccm_param.t_sys.enable_cavity_temp = 0; //
      HAL_GPIO_DeInit(GD32_TEMP_E_GPIO_Port, GD32_TEMP_E_Pin);
      GPIO_InitTypeDef GPIO_InitStruct = {0};
      /* GPIO Ports Clock Enable */
      __HAL_RCC_GPIOB_CLK_ENABLE();
      GPIO_InitStruct.Pin = GD32_TEMP_E_Pin;
      GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
      GPIO_InitStruct.Pull = GPIO_PULLUP;
      HAL_GPIO_Init(GD32_TEMP_E_GPIO_Port, &GPIO_InitStruct);
    }
  }

  #endif
}

bool user_abl_z_check(void)
{
  #if defined(STM32F407xx)
  bool result = false;

  if (0 == ccm_param.t_sys_data_current.IsMechanismLevel)
  {
    #if defined(E1_STEP_PIN) && (E1_STEP_PIN_ON > -1)
    result = (!digitalRead(E1_STEP_PIN));

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = (!digitalRead(E1_STEP_PIN));
    }

    #endif
  }
  else if (1 == ccm_param.t_sys_data_current.IsMechanismLevel)
  {
    result = !digitalRead(TEMP_0_PIN);

    if (result)
    {
      user_motor_delay_us(10);//防抖
      result = !digitalRead(TEMP_0_PIN);
    }
  }

  return result;
  #elif defined(STM32F429xx)

  if (BED_LEVEL_PRESSURE_SENSOR == ccm_param.t_sys_data_current.enable_bed_level)
  {
    if (P2_Pro_NEW == ccm_param.t_sys_data_current.model_id || P3_Pro == ccm_param.t_sys_data_current.model_id)
    {
      return user_motor_axis_endstop_read_min(Z_AXIS);
    }
    else if (K600 == ccm_param.t_sys_data_current.model_id || F400TP == ccm_param.t_sys_data_current.model_id)
    {
      return user_motor_axis_endstop_read_max(Z_AXIS);
    }
    else if (F300TP == ccm_param.t_sys_data_current.model_id)
    {
      USER_GPIO_GET(TEMP_CAVITY, GPIO_PIN_RESET);
    }
    else if (K5 == ccm_param.t_sys_data_current.model_id)
    {
      if (1 == ccm_param.t_sys_data_current.IsMechanismLevel) // 黑屏固件，机械调平
      {
        USER_GPIO_GET(TEMP_E, GPIO_PIN_RESET);
      }
    }
  }

  return false;
  #endif
}

#endif



