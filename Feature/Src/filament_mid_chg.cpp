#include "user_common_cpp.h"

#ifdef HAS_FILAMENT
#include "gcode.h"
#if defined(STM32F407xx)
  #include "gcode_global_params.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if defined(STM32F429xx)
void setConfirmLoadFilament(int _IsConfirmLoadFilament);
void setMidWayChangeMat(bool value);
#endif

#ifdef __cplusplus
} //extern "C" {
#endif
namespace feature_filament
{

#define NotStartChangeFilament 0
#define StartChangeFilament 1
#define ChangeFilamentFinish 5
#define HeatNotComplete 0
#define HeatComplete 1

  static int IsStartChangeFilament = NotStartChangeFilament;


  void mid_chg_confirm_load(void)
  {
    USER_EchoLogStr("ConfirmLoadFilamentValue ok!\r\n");//串口上传信息到上位机2017.7.6
#if defined(STM32F429xx)
    setConfirmLoadFilament(1);
#elif defined(STM32F407xx)
    gcode::is_confirm_load_filament = true;
#endif
  }

  void mid_chg_set_heat_status(void)
  {
    if (ccm_param.t_gui_p.m109_heating_complete)
    {
      ccm_param.t_gui_p.ChangeFilamentHeatStatus = HeatComplete;
    }
    else
    {
      ccm_param.t_gui_p.ChangeFilamentHeatStatus = HeatNotComplete;
    }
  }

  void mid_chg_start(void)
  {
#if defined(STM32F429xx)
    taskENTER_CRITICAL();
    print_status.is_mid_chg_filament = true;
    taskEXIT_CRITICAL();
    osDelay(200);
    ccm_param.t_gui_p.M600FilamentChangeStatus = gcode::m600_status;
    IsStartChangeFilament = StartChangeFilament;
    // 打开中途换料标志位
    setMidWayChangeMat(true);

    if (ccm_param.t_gui_p.IsNotHaveMatInPrint) //断料续打中的换料发送M601
    {
      user_send_internal_cmd((char *)"M601");   //换料
    }
    else
    {
      user_send_internal_cmd((char *)"M600");
    }

#elif defined(STM32F407xx)
    uint8_t _print_status = 0;

    if (print_status.is_printing)
    {
      _print_status = 1;
      print_status.is_printing = false;  //解决多次中途换料后，打印乱跑现象
    }
    else if (print_status.is_pause_printing)
    {
      _print_status = 2;
      print_status.is_pause_printing = false;  // 解决暂停打印后中途换料，中途换料时，暂停后续操作同时进行（目标温度下降为目标温度一半），导致进丝异常
    }

    ccm_param.t_gui_p.M600FilamentChangeStatus = gcode::m600_filament_change_status;
    IsStartChangeFilament = StartChangeFilament;
    // 打开中途换料标志位
    gcode::m600_is_midway_change_material = true;

    if (ccm_param.t_gui_p.IsNotHaveMatInPrint) //断料续打中的换料发送M601
    {
      user_send_internal_cmd("M601"); //换料
    }
    else
    {
      if (0 == _print_status)
      {
        user_send_internal_cmd("M600 S0"); //不是打印状态下换料，不会执行该逻辑
      }

      if (1 == _print_status)
      {
        user_send_internal_cmd("M600 S1"); //正常打印下换料
      }

      if (2 == _print_status)
      {
        user_send_internal_cmd("M600 S2");  //暂停打印下换料
      }
    }

#endif
  }

  void mid_chg_refresh_status(void)
  {
    if (gcode::m600_is_midway_change_material)
    {
      mid_chg_set_heat_status();
      ccm_param.t_gui_p.M600FilamentChangeStatus = gcode::m600_filament_change_status;
      if (StartChangeFilament == IsStartChangeFilament)
      {
#if defined(STM32F429xx)
        ccm_param.t_gui_p.M600FilamentChangeStatus = gcode::m600_status;

        if (M600_STATUS_FINISH == ccm_param.t_gui_p.M600FilamentChangeStatus)
        {
          IsStartChangeFilament = NotStartChangeFilament;
          gcode::m600_status = 0;
          setConfirmLoadFilament(0);
        }

#elif defined(STM32F407xx)
        

        if (ChangeFilamentFinish == ccm_param.t_gui_p.M600FilamentChangeStatus)
        {
          IsStartChangeFilament = NotStartChangeFilament;
          gcode::m600_filament_change_status = false;
          gcode::is_confirm_load_filament = false;
        }

#endif
      }
    }
  }

}



#endif // HAS_FILAMENT

