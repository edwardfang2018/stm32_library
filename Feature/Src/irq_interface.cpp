#include "user_common_cpp.h"
#include "temperature.h"
#include "stepper.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(STM32F407xx)
void TIM6_IRQHandler_process(void)
{
  sg_grbl::temperature_update();
}
#elif defined(STM32F429xx)
void TIM3_IRQHandler_process(void)
{
  sg_grbl::temperature_update();
}
#endif

void TIM4_IRQHandler_process(void)
{
  sg_grbl::st_process();
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if ((GPIO_Pin == GPIO_PIN_3) && ccm_param.t_sys_data_current.enable_powerOff_recovery) // PD3
  {
    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DRUG || flash_param_t.extruder_type == EXTRUDER_TYPE_LASER)
    {
      __HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
      return;
    }

    if ((print_status.is_printing && ccm_param.power_is_power_off != 1U) || ccm_param.power_is_data_change == 1)
    {
#if defined(STM32F429xx)

      if (HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3) == GPIO_PIN_SET)
#elif defined(STM32F407xx)
      if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_3) == GPIO_PIN_RESET)
#endif
      {
        if (ccm_param.power_is_data_change != 1)
        {
          memcpy((planner_running_status_t *)&ccm_param.flash_poweroff_recovery,
                  (planner_running_status_t *)&ccm_param.runningStatus[ccm_param.block_buffer_tail], sizeof(planner_running_status_t));
        }

        ccm_param.flash_poweroff_recovery.flag = 1;
        flash_poweroff_data_save();
        HAL_NVIC_SystemReset();
      }

      __HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
    }
  }
}

#ifdef __cplusplus
}
#endif









