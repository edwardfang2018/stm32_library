#ifndef IRQ_INTERFACE_H
#define IRQ_INTERFACE_H

#if defined(STM32F407xx)
  extern void TIM6_IRQHandler_process(void); ///< 定时器6，温度检测执行
#elif defined(STM32F429xx)
  extern void TIM3_IRQHandler_process(void); ///< 定时器3，温度检测执行
#endif
extern void TIM4_IRQHandler_process(void); ///< 定时器4，电机执行入口

#endif // IRQ_INTERFACE_H













