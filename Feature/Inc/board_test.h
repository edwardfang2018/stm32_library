#ifndef BOARD_TEST_H
#define BOARD_TEST_H

#ifdef HAS_BOARD_TEST

#ifdef __cplusplus
extern "C" {
#endif

void gui_board_test(void);
void board_test_init(void);
void board_test_main(void);
void board_test_motor_run(void);
void board_test_motor_check_endstop(void);

#ifdef __cplusplus
} //extern "C" {
#endif
#endif 
#endif // BOARD_TEST_H

