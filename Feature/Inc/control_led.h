#ifndef CONTROL_LED_H
#define CONTROL_LED_H

#ifdef HAS_LED_CONTROL

#ifdef __cplusplus
extern "C" {
#endif

extern bool feature_control_led_get_lighting_status(void);
extern void feature_control_led_set_lighting_status(bool value);
extern void feature_control_led(void);
extern void feature_control_led_caution_twinkle(uint32_t delay_time);

#ifdef __cplusplus
} //extern "C"
#endif

#endif

#endif // CONTROL_LED_H

