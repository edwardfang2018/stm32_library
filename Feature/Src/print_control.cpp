#include "user_common_cpp.h"
#ifdef HAS_PRINT_CONTROL
#if defined(STM32F429xx)
  #include "process_command.h"
  #include "gcode.h"
  #include "Configuration.h"
#elif defined(STM32F407xx)
  #include "sys_function.h"
  #include "view_common.h"
  #include "user_interface.h"
  #include "gcode.h"
#endif
#include "USBFileTransfer.h"
#include "temperature.h"
#ifdef __cplusplus
extern "C" {
#endif
#if defined(STM32F429xx)

#elif defined(STM32F407xx)
unsigned long starttime = 0;              /*!< 打印时间计数 */
extern char printfilepathname[MK_MAX_LFN];             /*!< 文件名称 */
bool IsPauseToCoolDown(void)
{
  return print_status.is_pause_to_cool_down;
}

#endif
t_print_status print_status;
namespace feature_print_control
{
  static volatile uint32_t print_start_time = 0;              /*!< 打印时间计数 */
  #if defined(STM32F429xx)
  static volatile bool is_check_idex_normal_mode = false;     /*!< 检测idex正常模式下打印单色、双色 */

  static void set_check_idex_normal_mode(void)
  {
    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL &&
        ccm_param.t_sys.is_idex_extruder == 1 &&
        ccm_param.t_sys.idex_print_type == IDEX_PRINT_TYPE_NORMAL)
    {
      is_check_idex_normal_mode = true;
    }
  }

  void check_idex_normal_mode(void)
  {
    if (is_check_idex_normal_mode && print_status.is_printing && flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL &&
        ccm_param.t_sys.is_idex_extruder == 1 && ccm_param.t_sys.idex_print_type == IDEX_PRINT_TYPE_NORMAL)
    {
      if (ccm_param.layer_count > 0)
      {
        if (sg_grbl::temperature_get_extruder_target(gcode::active_extruder) > 0.0f &&
            sg_grbl::temperature_get_extruder_target((gcode::active_extruder + 1) % EXTRUDERS) < 80.0f)
        {
          ccm_param.t_sys.idex_print_type_single_mode = 1;
        }
        else
        {
          ccm_param.t_sys.idex_print_type_single_mode = 0;
        }

        is_check_idex_normal_mode = false;
      }
    }
  }
  #endif
  static void print_status_reset(void)
  {
    print_status.is_printing = false;
    print_status.is_pause_printing = false;
    print_status.is_stop_printing = false;
    print_status.is_resume_printing = false;
    print_status.is_finish_print = false;
    print_status.is_mid_chg_filament = false;
    print_status.is_poweroff_recovery_print = false;
    print_status.is_print_sd_file = false;
    print_status.is_pause_to_cool_down = false;
  }

  //是否在加热
  int print_is_heating(void)
  {
    if (!ccm_param.t_custom_services.disable_hot_bed)
    {
      if ((int)sg_grbl::temperature_get_bed_target())
      {
        return 1;
      }
    }

    if ((int)sg_grbl::temperature_get_extruder_target(0))
    {
      return 1;
    }

    return 0;
  }

  //开始打印
  static void print_start(void)
  {
    (void)memset(ccm_param.file_read_buf, 0, sizeof(ccm_param.file_read_buf)); //clear buffer
    ccm_param.t_gui_p.m305_is_force_verify = 0;
    ccm_param.t_gui.file_size = 0;
    ccm_param.t_gui.printfile_size = 0;
    ccm_param.t_gui_p.IsNotHaveMatInPrint = 0;
    ccm_param.file_read_buf_index = FILE_READ_SIZE;
    // 重置联机打印状态
    SetIsUSBPrintFinished(false);
    SetIsUSBPrintStop(false);
    SetIsUSBPrintPause(false);
    #if defined(STM32F429xx)
    set_check_idex_normal_mode();
    #endif

    if (feature_print_control::file_open())
    {
      #if defined(STM32F429xx)
      // 文件打开成功，发送指令M2000设置打印状态
      print_start_time = xTaskGetTickCount();
      ccm_param.t_sys.idex_print_type = flash_param_t.idex_print_type;
      ccm_param.t_sys.mix_print_type = flash_param_t.mix_print_type;
      stepper_quick_stop(); // 电机快速停止

      if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
      {
        ccm_param.layer_count = ccm_param.t_power_off.layer_count;
        ccm_param.current_layer = ccm_param.t_power_off.current_layer;
      }
      else
      {
        ccm_param.layer_count = 0;
        ccm_param.current_layer = 0;
      }

      user_send_file_cmd((char *)"M2000", 0, ccm_param.layer_count, ccm_param.current_layer);
      #elif defined(STM32F407xx)
      user_fan_control_nozzle_heat_block(true);  // 打开混色5v风扇
      stepper_quick_stop(); // 电机快速停止

      if (!print_status.is_poweroff_recovery_print && !ccm_param.t_sys_data_current.IsLaser)
      {
        user_send_internal_cmd("G92 X0 Y0 A1 isInternal");
      }

      if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
      {
        (void)memcpy(ccm_param.power_path_file_name, printfilepathname, strlen(printfilepathname));
        ccm_param.power_path_file_name[strlen(printfilepathname)] = '\0';
        print_status.is_poweroff_recovery_print = false; // 完成恢复操作，重置断电恢复操作标志
      }

      print_status.is_printing = true;// 发指令，设置打印状态，开始读U盘数据
      task_read_udisk_release();
      sys_os_delay(50);
      #endif
    }

    #if defined(STM32F429xx)

    if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
    {
      print_status.is_poweroff_recovery_print = false; // 完成恢复操作，重置断电恢复操作标志
    }

    ccm_param.t_sys.enable_color_buf = 0;
    #endif
  }

  //开始打印
  void power_rec_print_start(void)
  {
    print_status_reset();
    print_status.is_poweroff_recovery_print = true;
    flash_poweroff_data_reset();
    print_start();
  }


  void file_print_start(void)
  {
    print_status_reset();         // 重置打印变量
    flash_config_set_flag_save(); // 保存文件名
    flash_poweroff_data_reset();  // 重置断电flash

    if (!ccm_param.t_sys_data_current.enable_powerOff_recovery)
    {
      sg_grbl::temperature_set_extruder_target(0, 0);
      sg_grbl::temperature_set_extruder_target(0, 1);
      sg_grbl::temperature_set_bed_target(0);
    }

    print_start();
    ccm_param.t_gui_p.m109_heating_complete = false;  //打印时重置为还没有加热完成
    feature_filament::mid_chg_set_heat_status(); //解决一开始就跳转到有中途换料的界面的问题
  }

  void set_file_print_finish(void)
  {
    print_status_reset();
    print_status.is_finish_print = true;   // 设置打印完成标志为true，用于gui页面跳转
  }

  void set_file_print_start(void)
  {
    print_status_reset();
    print_status.is_printing = true;   // 设置打印完成标志为true，用于gui页面跳转
  }

  unsigned int print_time(void)
  {
    #if defined(STM32F429xx)
    return (unsigned int)((xTaskGetTickCount() / 1000 - print_start_time / 1000) + ccm_param.t_sys.print_time_save);
    #elif defined(STM32F407xx)
    return (unsigned int)(((sys_task_get_tick_count() / 1000) - (starttime / 1000)));// + ccm_param.t_sys.print_time_save);
    #endif
  }
}


#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_PRINT_CONTROL





