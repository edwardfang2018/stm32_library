#include "user_common_cpp.h"
#ifdef HAS_POWER_RECOVERY

#if defined(STM32F429xx)
  #include "controlxyz.h"
  #include "process_m_code.h"
  #include "process_command.h"
  #include "gcode.h"
  #include "planner.h"
  #ifdef ENABLE_GUI_LVGL
    #include "custom.h"
  #endif
  static volatile uint8_t _feature_pow_rec_status = 0;
  extern volatile float mix_eb_value_save;
  extern volatile int mix_current_layer_save;
#elif defined(STM32F407xx)
  #include "controlxyz.h"
  #include "Alter.h"
  #include "gcode.h"
  #include "stepper.h"
  #include "planner.h"
  #include "gcode_global_params.h"
  static bool isXYHome = false;
#endif

#ifdef __cplusplus
extern "C" {
#endif

static volatile bool _feature_pow_rec_is_start = false;                       /*!< 是否为断电恢复操作 */
static volatile bool _feature_pow_rec_is_finish = false;                 /*!< 是否为断电恢复操作 */

void feature_pow_rec_ready_to_print(void)
{
  if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
  {
    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DRUG || flash_param_t.extruder_type == EXTRUDER_TYPE_LASER)
      return;

    #if defined(STM32F429xx)
    _feature_pow_rec_is_start = 1;
    ccm_param.power_is_power_off = 1U; //标志为断电状态
    #elif defined(STM32F407xx)
    stepper_quick_stop(); // 电机快速停止
    #if LASER_MODE

    if (ccm_param.t_sys_data_current.IsLaser)
      ccm_param.t_gui_p.m109_heating_complete = 1U;
    else
    #endif
      ccm_param.t_gui_p.m109_heating_complete = 0U; //设置为未加热

    if (ccm_param.t_power_off.sd_position != 0U)
    {
      ccm_param.motion_3d.updown_g28_first_time = 0;
      user_send_internal_cmd("G90 isInternal");
      user_send_internal_cmd("M82 isInternal");
      #if LASER_MODE

      if (!ccm_param.t_sys_data_current.IsLaser)
      #endif
      {
        user_send_internal_cmd("M109 S180 isInternal");// 先加热到180度，再移动喷嘴防止喷嘴与打印模具粘在一起。
      }

      z_down_to_bottom(); // Z下降到底部 XY归零命令
    }
    else
    {
      ccm_param.motion_3d.updown_g28_first_time = 0U;
      user_send_internal_cmd("G90 isInternal");
      user_send_internal_cmd("M82 isInternal");
      user_send_internal_cmd("G28 isInternal");
    }

    _feature_pow_rec_is_start = 1;
    isXYHome = false;
    ccm_param.power_is_power_off = 1U; //标志为断电状态，防止把当前sdPos写入到poweroff_data中，解决断电多次重新打印现象
    #endif
  }
}

#if defined(STM32F407xx)
static void _feature_pow_rec_finish(void)
{
  if (_feature_pow_rec_is_finish)
  {
    ccm_param.t_gui_p.IsFinishedPowerOffRecoverReady = 1U; // UI界面更新标志位

    if (0U == ccm_param.t_power_off.is_serial)
    {
      feature_print_control::power_rec_print_start(); //开始从文件读取内容继续去打印
    }

    _feature_pow_rec_is_finish = false;
    ccm_param.power_is_power_off = 0U;
  }
}

static void _feature_pow_rec_temp_set(void)
{
  USER_SEND_INTERNAL_CMD_BUF("M140 S%d isInternal", (int)ccm_param.t_power_off.target_bed_temp);//设置Z最大位置
  USER_SEND_INTERNAL_CMD_BUF("M104 S%d isInternal", (int)ccm_param.t_power_off.target_extruder0_temp);//设置Z最大位置
}

static void _feature_pow_rec_close_to_saved_z(void)
{
  static char gcodeG1CommandBuf[50] = {0};
  (void)memset(gcodeG1CommandBuf, 0, sizeof(gcodeG1CommandBuf));
  float zUpValue = ccm_param.t_sys_data_current.poweroff_rec_z_max_value;

  if (ccm_param.t_sys_data_current.poweroff_rec_z_max_value > (50.0F + ccm_param.t_power_off.axis_position[Z_AXIS])) // float
  {
    zUpValue = 50.0F + ccm_param.t_power_off.axis_position[Z_AXIS]; // float
  }

  if ((M4141 == ccm_param.t_sys_data_current.model_id) || (M4141S_NEW == ccm_param.t_sys_data_current.model_id) || (M4141S == ccm_param.t_sys_data_current.model_id))
  {
    (void)snprintf(gcodeG1CommandBuf, sizeof(gcodeG1CommandBuf), "G1 F200 Z%.2f I0 H0 isInternal", zUpValue);
  }
  else
  {
    (void)snprintf(gcodeG1CommandBuf, sizeof(gcodeG1CommandBuf), "G1 F600 Z%.2f I0 H0 isInternal", zUpValue);
  }

  user_send_internal_cmd(gcodeG1CommandBuf);//设置Z最大位置
}

#endif

static void _feature_pow_rec_eb_set(void)
{
  #if defined(STM32F429xx)
  USER_SEND_INTERNAL_CMD_BUF("M2003 S0");    // 关闭坐标转换

  if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
  {
    if (flash_param_t.idex_print_type == IDEX_PRINT_TYPE_COPY || flash_param_t.idex_print_type == IDEX_PRINT_TYPE_MIRROR)
    {
      USER_SEND_INTERNAL_CMD_BUF("G92 E0 B0");          // 设置eb位置
      USER_SEND_INTERNAL_CMD_BUF("G1 F150 E8 B8 D1");      // eb各自运动8mm
      USER_SEND_INTERNAL_CMD_BUF("G92 E%f B%f", ccm_param.t_power_off.axis_real_position[E_AXIS], ccm_param.t_power_off.axis_real_position[B_AXIS]); // 设置EB坐标
    }
    else if (flash_param_t.idex_print_type == IDEX_PRINT_TYPE_NORMAL)
    {
      USER_SEND_INTERNAL_CMD_BUF("G92 E0 B0");          // 设置eb位置

      if (ccm_param.t_sys_data_current.enable_color_mixing)
      {
        USER_SEND_INTERNAL_CMD_BUF("G1 F150 E8 B8 D1");      // eb各自运动8mm
      }
      else
      {
        USER_SEND_INTERNAL_CMD_BUF("G1 F150 E16 B0 D1");      // eb各自运动8mm
      }

      USER_SEND_INTERNAL_CMD_BUF("G92 %c%f %c%f", axis_codes[E_AXIS], ccm_param.t_power_off.axis_real_position[E_AXIS],
                                 axis_codes[B_AXIS], ccm_param.t_power_off.axis_real_position[B_AXIS]); // 设置EB坐标
    }
  }
  else
  {
    USER_SEND_INTERNAL_CMD_BUF("G92 E0 B0");          // 设置eb位置

    if (ccm_param.t_sys_data_current.enable_color_mixing)
    {
      USER_SEND_INTERNAL_CMD_BUF("G1 F150 E8 B8 D1");      // eb各自运动8mm
    }
    else
    {
      USER_SEND_INTERNAL_CMD_BUF("G1 F150 E16 B0 D1");      // eb各自运动8mm
    }

    USER_SEND_INTERNAL_CMD_BUF("G92 %c%f %c%f", axis_codes[E_AXIS], ccm_param.t_power_off.axis_real_position[E_AXIS],
                               axis_codes[B_AXIS], ccm_param.t_power_off.axis_real_position[B_AXIS]); // 设置EB坐标
  }

  USER_SEND_INTERNAL_CMD_BUF("M2003 S1");            // 开启坐标转换
  #elif defined(STM32F407xx)
  //  if (0U != ccm_param.t_power_off.blockdetectflag) //20170927,堵料时断电续打须进丝一段时间
  //  {
  //    user_send_internal_cmd("G92 E0 isInternal");
  //    user_send_internal_cmd("G1 F150 E150 H0 isInternal");
  //    ccm_param.t_power_off.blockdetectflag = 0U;
  //  }
  eb_compensate_16mm(ccm_param.t_sys_data_current.enable_color_mixing);
  g92_set_axis_position(static_cast<int>(E_AXIS), ccm_param.t_power_off.axis_position[E_AXIS]);

  if (0U != ccm_param.t_sys_data_current.enable_color_mixing)
  {
    g92_set_axis_position(static_cast<int>(B_AXIS), ccm_param.t_power_off.axis_position[B_AXIS]);
  }

  #endif
}

static void _feature_pow_rec_z_set(void)
{
  #if defined(STM32F429xx)

  if (P3_Pro == ccm_param.t_sys_data_current.model_id)
  {
    USER_SEND_INTERNAL_CMD_BUF("M2003 S0"); // 关闭坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS]); // 移动Z
    USER_SEND_INTERNAL_CMD_BUF("G1 F%f R%u",
                               ccm_param.t_power_off.feed_rate > 2400 ?
                               2400 :
                               ccm_param.t_power_off.feed_rate, ccm_param.t_power_off.sd_position); // 设置移动速度，断电文件位置
    USER_SEND_INTERNAL_CMD_BUF("M2003 S1");    // 开启坐标转换

    if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
    {
      if (flash_param_t.idex_print_type == IDEX_PRINT_TYPE_COPY || flash_param_t.idex_print_type == IDEX_PRINT_TYPE_MIRROR)
      {
        USER_SEND_INTERNAL_CMD_BUF("T0 S-1"); // 设置当前喷嘴id
        USER_SEND_INTERNAL_CMD_BUF("G92 X%f I%f Y%f Z%f D1",
                                   ccm_param.t_power_off.axis_position[X_AXIS],
                                   ccm_param.t_power_off.axis_position[X2_AXIS],
                                   ccm_param.t_power_off.axis_position[Y_AXIS],
                                   ccm_param.t_power_off.axis_position[Z_AXIS]);
        USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f I%f Y%f Z%f D1",
                                   ccm_param.t_power_off.axis_position[X_AXIS],
                                   ccm_param.t_power_off.axis_position[X2_AXIS],
                                   ccm_param.t_power_off.axis_position[Y_AXIS],
                                   ccm_param.t_power_off.axis_position[Z_AXIS]);
      }
      else
      {
        USER_SEND_INTERNAL_CMD_BUF("T%d S-1", ccm_param.t_power_off.active_extruder); // 设置当前喷嘴id
        USER_SEND_INTERNAL_CMD_BUF("G92 X%f Y%f Z%f D1",
                                   ccm_param.t_power_off.axis_position[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                                   ccm_param.t_power_off.axis_position[Y_AXIS],
                                   ccm_param.t_power_off.axis_position[Z_AXIS]);
        USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f Z%f D1",
                                   ccm_param.t_power_off.axis_position[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                                   ccm_param.t_power_off.axis_position[Y_AXIS],
                                   ccm_param.t_power_off.axis_position[Z_AXIS]);
      }
    }
    else
    {
      USER_SEND_INTERNAL_CMD_BUF("T%d S-1", ccm_param.t_power_off.active_extruder); // 设置当前喷嘴id
      USER_SEND_INTERNAL_CMD_BUF("G92 X%f Y%f Z%f D1",
                                 ccm_param.t_power_off.axis_position[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                                 ccm_param.t_power_off.axis_position[Y_AXIS],
                                 ccm_param.t_power_off.axis_position[Z_AXIS]);
      USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f Z%f D1",
                                 ccm_param.t_power_off.axis_position[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                                 ccm_param.t_power_off.axis_position[Y_AXIS],
                                 ccm_param.t_power_off.axis_position[Z_AXIS]);
    }
  }
  else if (F300TP == ccm_param.t_sys_data_current.model_id)
  {
    USER_SEND_INTERNAL_CMD_BUF("M2003 S0"); // 关闭坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS]); // 移动Z
    USER_SEND_INTERNAL_CMD_BUF("G1 F%f R%u",
                               ccm_param.t_power_off.feed_rate > 2400 ?
                               2400 :
                               ccm_param.t_power_off.feed_rate, ccm_param.t_power_off.sd_position); // 设置移动速度，断电文件位置
    USER_SEND_INTERNAL_CMD_BUF("M2003 S1");    // 开启坐标转换
  }
  else if (K5 == ccm_param.t_sys_data_current.model_id)
  {
    USER_SEND_INTERNAL_CMD_BUF("M2003 S0"); // 关闭坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS]); // 移动Z
    USER_SEND_INTERNAL_CMD_BUF("G1 F%f R%u",
                               ccm_param.t_power_off.feed_rate > 2400 ?
                               2400 :
                               ccm_param.t_power_off.feed_rate, ccm_param.t_power_off.sd_position); // 设置移动速度，断电文件位置
    USER_SEND_INTERNAL_CMD_BUF("M2003 S1");    // 开启坐标转换
  }

  #elif defined(STM32F407xx)

  // 需要判断是否归零
  if ((ccm_param.motion_3d_model.xyz_move_max_pos[0] < ccm_param.t_power_off.axis_position[X_AXIS]) || (ccm_param.motion_3d_model.xyz_move_max_pos[1] < ccm_param.t_power_off.axis_position[Y_AXIS]))
  {
    user_send_internal_cmd("G28 isInternal");
  }
  else
  {
    static char gcodeG1ZCommandBuf[50] = {0};
    (void)memset(gcodeG1ZCommandBuf, 0, sizeof(gcodeG1ZCommandBuf));

    if ((M4141 == ccm_param.t_sys_data_current.model_id) || (M4141S_NEW == ccm_param.t_sys_data_current.model_id) || (M4141S == ccm_param.t_sys_data_current.model_id))
    {
      (void)snprintf(gcodeG1ZCommandBuf, sizeof(gcodeG1ZCommandBuf), "G1 F400 Z%f P%u I0 H0 isInternal", ccm_param.t_power_off.axis_position[Z_AXIS], ccm_param.t_power_off.sd_position); //添加上文件位置
    }
    else
    {
      (void)snprintf(gcodeG1ZCommandBuf, sizeof(gcodeG1ZCommandBuf), "G1 F600 Z%f P%u I0 H0 isInternal", ccm_param.t_power_off.axis_position[Z_AXIS], ccm_param.t_power_off.sd_position); //添加上文件位置
    }

    user_send_internal_cmd(gcodeG1ZCommandBuf);//Z位置
    USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f H0 isInternal", ccm_param.t_power_off.axis_position[X_AXIS], ccm_param.t_power_off.axis_position[Y_AXIS]);//Z位置
  }

  if (ccm_param.t_power_off.feed_rate > 2400.0F)
  {
    ccm_param.t_power_off.feed_rate = 2400.0F; // 限制移动速度最大为40mm/s
  }

  USER_SEND_INTERNAL_CMD_BUF("G1 F%f H0 isInternal", ccm_param.t_power_off.feed_rate * 60.0f * 100.0f / ccm_param.t_power_off.feed_multiply);//出料速度
  #endif
}

static void _feature_pow_rec_heating(void)
{
  if (!ccm_param.t_custom_services.disable_hot_bed)
  {
    if (ccm_param.t_power_off.target_bed_temp > 50)
    {
      USER_SEND_INTERNAL_CMD_BUF("M190 S50");
      USER_SEND_INTERNAL_CMD_BUF("M140 S%d", (int)ccm_param.t_power_off.target_bed_temp);
    }
    else
    {
      USER_SEND_INTERNAL_CMD_BUF("M190 S%d", (int)ccm_param.t_power_off.target_bed_temp);
    }
  }

  #if defined(STM32F429xx)
  USER_SEND_INTERNAL_CMD_BUF("M104 T0 S%d", (int)ccm_param.t_power_off.target_extruder0_temp);
  USER_SEND_INTERNAL_CMD_BUF("M104 T1 S%d", (int)ccm_param.t_power_off.target_extruder1_temp);
  USER_SEND_INTERNAL_CMD_BUF("M109 T0 S%d", (int)ccm_param.t_power_off.target_extruder0_temp);
  USER_SEND_INTERNAL_CMD_BUF("M109 T1 S%d", (int)ccm_param.t_power_off.target_extruder1_temp);
  #elif defined(STM32F407xx)
  USER_SEND_INTERNAL_CMD_BUF("M109 S%d isInternal", (int)ccm_param.t_power_off.target_extruder0_temp);//喷嘴加温命令
  #endif
}

#if defined(STM32F429xx)
static void _feature_pow_rec_xy_set(void)
{
  USER_SEND_INTERNAL_CMD_BUF("M2003 S0");

  if (flash_param_t.extruder_type == EXTRUDER_TYPE_DUAL && ccm_param.t_sys.is_idex_extruder == 1)
  {
    if (flash_param_t.idex_print_type == IDEX_PRINT_TYPE_COPY || flash_param_t.idex_print_type == IDEX_PRINT_TYPE_MIRROR)
    {
      USER_SEND_INTERNAL_CMD_BUF("T0 S-1");
      USER_SEND_INTERNAL_CMD_BUF("G92 X%f I%f Y%f D1",
                                 ccm_param.motion_3d_model.xyz_home_pos[X_AXIS],
                                 ccm_param.motion_3d_model.xyz_home_pos[X2_AXIS],
                                 ccm_param.motion_3d_model.xyz_home_pos[Y_AXIS]);
      USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f I%f Y%f D1",
                                 ccm_param.t_power_off.axis_real_position[X_AXIS],
                                 ccm_param.t_power_off.axis_real_position[X2_AXIS],
                                 ccm_param.t_power_off.axis_real_position[Y_AXIS]);
    }
    else
    {
      USER_SEND_INTERNAL_CMD_BUF("T%d S-1", ccm_param.t_power_off.active_extruder);
      USER_SEND_INTERNAL_CMD_BUF("G92 X%f Y%f D1",
                                 ccm_param.motion_3d_model.xyz_home_pos[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                                 ccm_param.motion_3d_model.xyz_home_pos[Y_AXIS]);
      USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f D1",
                                 ccm_param.t_power_off.axis_real_position[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                                 ccm_param.t_power_off.axis_real_position[Y_AXIS]);
    }
  }
  else
  {
    USER_SEND_INTERNAL_CMD_BUF("T%d S-1", ccm_param.t_power_off.active_extruder);
    USER_SEND_INTERNAL_CMD_BUF("G92 X%f Y%f D1",
                               ccm_param.motion_3d_model.xyz_home_pos[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                               ccm_param.motion_3d_model.xyz_home_pos[Y_AXIS]);
    USER_SEND_INTERNAL_CMD_BUF("G1 F2400 X%f Y%f D1",
                               ccm_param.t_power_off.axis_real_position[ccm_param.t_power_off.active_extruder == 0 ? X_AXIS : X2_AXIS],
                               ccm_param.t_power_off.axis_real_position[Y_AXIS]);
  }

  USER_SEND_INTERNAL_CMD_BUF("M2003 S1");
}

static void _feature_pow_rec_prepare(void)
{
  // 先加热到150度，再移动喷嘴防止喷嘴与打印模具粘在一起。
  if (ccm_param.t_power_off.target_extruder0_temp >= 20)
    USER_SEND_INTERNAL_CMD_BUF("M104 T0 S%d", (int)ccm_param.t_power_off.target_extruder0_temp - 20);

  if (ccm_param.t_power_off.target_extruder1_temp >= 20)
    USER_SEND_INTERNAL_CMD_BUF("M104 T1 S%d", (int)ccm_param.t_power_off.target_extruder1_temp - 20);

  if (ccm_param.t_power_off.target_extruder0_temp >= 20)
    USER_SEND_INTERNAL_CMD_BUF("M109 T0 S%d", (int)ccm_param.t_power_off.target_extruder0_temp - 20);

  if (ccm_param.t_power_off.target_extruder1_temp >= 20)
    USER_SEND_INTERNAL_CMD_BUF("M109 T1 S%d", (int)ccm_param.t_power_off.target_extruder1_temp - 20);

  // P3_Pro断电Z不动
  if (P2_Pro_NEW == ccm_param.t_sys_data_current.model_id || P3_Pro == ccm_param.t_sys_data_current.model_id)
  {
    // 设置当前Z位置
    USER_SEND_INTERNAL_CMD_BUF("M2003 S0");            // 关闭坐标转换

    if (user_is_float_data_equ(ccm_param.t_power_off.z_real_change_value, 0.0f))
    {
      USER_SEND_INTERNAL_CMD_BUF("G92 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS]);
      USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS] + 10.0f);      // Z增加60mm
      flash_poweroff_data_reset();
      ccm_param.power_is_data_change = 1;
      ccm_param.flash_poweroff_recovery.z_real_change_value = ccm_param.t_power_off.axis_real_position[Z_AXIS] + 10.0f;
    }
    else
    {
      USER_SEND_INTERNAL_CMD_BUF("G92 Z%f", ccm_param.t_power_off.z_real_change_value);
    }

    USER_SEND_INTERNAL_CMD_BUF("M2003 S1");            // 开启坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G90");                 // 关闭相对模式
    USER_SEND_INTERNAL_CMD_BUF("M82");
    // XY回原点
    xy_to_zero();
  }
  else if (F300TP == ccm_param.t_sys_data_current.model_id)
  {
    // 设置当前Z位置
    USER_SEND_INTERNAL_CMD_BUF("M2003 S0");            // 关闭坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G92 Z0");
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z999");
    USER_SEND_INTERNAL_CMD_BUF("M400");
    USER_SEND_INTERNAL_CMD_BUF("G92 Z%f",  flash_param_t.poweroff_rec_z_max_value);
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS] + 20); // 移动Z
    USER_SEND_INTERNAL_CMD_BUF("M2003 S1");            // 开启坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G90");                 // 关闭相对模式
    USER_SEND_INTERNAL_CMD_BUF("M82");
    // XY回原点
    xy_to_zero();
  }
  else if (K5 == ccm_param.t_sys_data_current.model_id)
  {
    // 设置当前Z位置
    USER_SEND_INTERNAL_CMD_BUF("M2003 S0");            // 关闭坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G92 Z0");
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z999");
    USER_SEND_INTERNAL_CMD_BUF("M400");
    USER_SEND_INTERNAL_CMD_BUF("G92 Z%f",  flash_param_t.poweroff_rec_z_max_value);
    USER_SEND_INTERNAL_CMD_BUF("G1 F600 Z%f", ccm_param.t_power_off.axis_real_position[Z_AXIS] + 20); // 移动Z
    USER_SEND_INTERNAL_CMD_BUF("M2003 S1");            // 开启坐标转换
    USER_SEND_INTERNAL_CMD_BUF("G90");                 // 关闭相对模式
    USER_SEND_INTERNAL_CMD_BUF("M82");
    // XY回原点
    xy_to_zero();
  }
}
#endif

static void _feature_pow_rec_process_loop(void)
{
  #if defined(STM32F429xx)

  if (_feature_pow_rec_status == 0)   //加热完成和Z轴向下完成
  {
    #ifdef ENABLE_GUI_LVGL
    lvgl_custom_param.power_rec_status = CUSTOM_POWER_REC_STATUS_HOME_Z_DOWN;
    #endif
    _feature_pow_rec_status = 1;
    ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE = false; //设置为归零
    stepper_quick_stop(); // 电机快速停止
    ccm_param.t_gui_p.m109_heating_complete = false; //设置为未加热
    _feature_pow_rec_prepare(); // 准备操作，预热，xy归零
  }
  else if (_feature_pow_rec_status == 1 && ccm_param.t_gui_p.m109_heating_complete && 0 == sg_grbl::planner_moves_planned())
  {
    #ifdef ENABLE_GUI_LVGL
    lvgl_custom_param.power_rec_status = CUSTOM_POWER_REC_STATUS_HEATING;
    #endif
    _feature_pow_rec_status = 2;
    ccm_param.t_gui_p.m109_heating_complete = false; //设置为未加热
    _feature_pow_rec_heating(); //设置目标温度，等待
  }
  else if (_feature_pow_rec_status == 2 && ccm_param.t_gui_p.m109_heating_complete)
  {
    #ifdef ENABLE_GUI_LVGL
    lvgl_custom_param.power_rec_status = CUSTOM_POWER_REC_STATUS_REC_POS;
    #endif
    _feature_pow_rec_status = 3;
    _feature_pow_rec_eb_set(); // 加热完成设置eb值
  }
  else if (_feature_pow_rec_status == 3 && 0 == sg_grbl::planner_moves_planned())     //加热完成和XY轴归零完成和Z轴向下归零完成
  {
    _feature_pow_rec_status = 4;
    _feature_pow_rec_xy_set(); // 移动xy
  }
  else if (_feature_pow_rec_status == 4 && 0 == sg_grbl::planner_moves_planned())     //加热完成和XY轴归零完成和Z轴向下归零完成
  {
    _feature_pow_rec_status = 5;
    _feature_pow_rec_z_set(); // 移动Z
  }
  else if (_feature_pow_rec_status == 5 && 0 == sg_grbl::planner_moves_planned())     //加热完成和XY轴归零完成和Z轴向下归零完成
  {
    feature_control_set_fan_speed(ccm_param.t_power_off.fan_speed);         //风扇速度
    SetFeedMultiply(ccm_param.t_power_off.feed_multiply); //打印速度
    gcode::current_layer = ccm_param.t_power_off.current_layer;
    gcode::layer_count = ccm_param.t_power_off.layer_count;
    _feature_pow_rec_status = 0;
    _feature_pow_rec_is_finish = true;
    #ifdef ENABLE_GUI_LVGL
    lvgl_custom_param.power_rec_status = CUSTOM_POWER_REC_STATUS_FINISH;
    #endif

    if (flash_param_t.extruder_type == EXTRUDER_TYPE_MIX)
    {
      mix_eb_value_save = ccm_param.t_power_off.axis_real_position[E_AXIS] + ccm_param.t_power_off.axis_real_position[B_AXIS];
      mix_current_layer_save = -1;
    }
  }

  #elif defined(STM32F407xx)
  static uint8_t powerOffRecoverPrintStatus = 0U;

  if ((powerOffRecoverPrintStatus == 0U) && (1U == ccm_param.t_gui_p.m109_heating_complete) && (0 == sg_grbl::planner_moves_planned()))   //加热完成和Z轴向下完成
  {
    ccm_param.t_gui_p.G28_ENDSTOPS_COMPLETE = 1U; //设置为归零
    ccm_param.motion_3d.updown_g28_first_time = 1U; // 设置已经执行了上下共限位归零操作
    z_check_and_set_bottom(ccm_param.motion_3d.enable_poweroff_up_down_min_min, ccm_param.t_sys_data_current.poweroff_rec_z_max_value); // 检测z底部位置
    #if LASER_MODE

    if (!ccm_param.t_sys_data_current.IsLaser)
    #endif
    {
      USER_SEND_INTERNAL_CMD_BUF("M140 S%d", (int)ccm_param.t_power_off.target_bed_temp);
      USER_SEND_INTERNAL_CMD_BUF("M104 S%d", (int)ccm_param.t_power_off.target_extruder0_temp);
    }

    powerOffRecoverPrintStatus = 1U;
    _feature_pow_rec_is_finish = false;
  }
  else if ((powerOffRecoverPrintStatus == 1U) && (0 == sg_grbl::planner_moves_planned()))
  {
    _feature_pow_rec_close_to_saved_z();
    powerOffRecoverPrintStatus = 2U;
  }
  else if ((powerOffRecoverPrintStatus == 2U) && (0 == sg_grbl::planner_moves_planned()))
  {
    #if LASER_MODE

    if (!ccm_param.t_sys_data_current.IsLaser)
    #endif
    {
      ccm_param.t_gui_p.m109_heating_complete = 0U;  //设置为未加热
      _feature_pow_rec_heating();
    }

    powerOffRecoverPrintStatus = 3U;
  }
  else if ((powerOffRecoverPrintStatus == 3U) && (1U == ccm_param.t_gui_p.m109_heating_complete))     //加热完成和XY轴归零完成和Z轴向下归零完成
  {
    #if LASER_MODE

    if (!ccm_param.t_sys_data_current.IsLaser)
    #endif
      _feature_pow_rec_eb_set();

    powerOffRecoverPrintStatus = 4U;
  }
  else if ((powerOffRecoverPrintStatus == 4U) && (0 == sg_grbl::planner_moves_planned()))     //加热完成和XY轴归零完成和Z轴向下归零完成
  {
    _feature_pow_rec_z_set();

    if (0U != ccm_param.t_power_off.is_serial)
    {
      USER_SEND_INTERNAL_CMD_BUF("M1005 S6");//打开串口打印状态
    }

    powerOffRecoverPrintStatus = 5U;
  }
  else if ((powerOffRecoverPrintStatus == 5U) && (0 == sg_grbl::planner_moves_planned()))     //加热完成和XY轴归零完成和Z轴向下归零完成
  {
    feature_control_set_fan_speed(ccm_param.t_power_off.fan_speed);         //风扇速度
    gcode::feed_multiply = ccm_param.t_power_off.feed_multiply; //打印速度
    powerOffRecoverPrintStatus = 0U;
    _feature_pow_rec_is_finish = true;
    #if LASER_MODE

    if (ccm_param.t_sys_data_current.IsLaser)
      ccm_param.t_gui_p.m109_heating_complete = 0U;  //设置为未加热

    #endif
  }
  else
  {
    // TODO
  }

  #endif
}

void feature_pow_rec_process(void)
{
  if (ccm_param.t_sys_data_current.enable_powerOff_recovery)
  {
    #if defined(STM32F429xx)

    if (_feature_pow_rec_is_start)
    {
      if (ccm_param.t_power_off.sd_position != 0)
      {
        _feature_pow_rec_process_loop();
      }
      else
      {
        _feature_pow_rec_is_finish = true;
      }

      if (_feature_pow_rec_is_finish)
      {
        ccm_param.t_gui_p.IsFinishedPowerOffRecoverReady = 1; // UI界面更新标志位
        USER_SEND_INTERNAL_CMD_BUF("M2003 S1");
        USER_SEND_INTERNAL_CMD_BUF("G21");
        USER_SEND_INTERNAL_CMD_BUF("G90");
        USER_SEND_INTERNAL_CMD_BUF("M82");
        feature_print_control::power_rec_print_start(); //开始从文件读取内容继续去打印
        _feature_pow_rec_is_finish = false;
        _feature_pow_rec_is_start = 0;
        ccm_param.power_is_power_off = 0U;
        ccm_param.power_is_data_change = 0;
        feature_pow_data_reset_flag();
      }
    }
    else
    {
      _feature_pow_rec_status = 0;
      _feature_pow_rec_is_finish = false;
    }

    #elif defined(STM32F407xx)
    static uint8_t isStartPowerOffRecoverPrint = 0;

    if ((0U != _feature_pow_rec_is_start) && (0U == isXYHome) && (0 == sg_grbl::planner_moves_planned()))
    {
      xy_to_zero();
      sg_grbl::st_synchronize();
      isXYHome = true;
    }

    if ((0U != _feature_pow_rec_is_start) && (0 != isXYHome) && (0 == sg_grbl::planner_moves_planned())) // 等待平台降到最低处
    {
      isStartPowerOffRecoverPrint = 1U;
      _feature_pow_rec_is_start = 0;
    }

    if (0U != isStartPowerOffRecoverPrint)
    {
      if (ccm_param.t_power_off.sd_position != 0U)
      {
        _feature_pow_rec_process_loop();
      }
      else
      {
        if (0U != ccm_param.t_power_off.is_serial)
        {
          _feature_pow_rec_temp_set();

          if (sg_grbl::planner_moves_planned() > 0)
          {
            return;
          }
        }

        _feature_pow_rec_is_finish = true;
      }

      if (0U != _feature_pow_rec_is_finish)
      {
        isStartPowerOffRecoverPrint = 0U;
      }
    }

    _feature_pow_rec_finish();
    #endif
  }
}

#ifdef __cplusplus
} //extern "C" {
#endif

#endif // HAS_POWER_RECOVERY













